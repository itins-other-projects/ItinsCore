package ua.com.itinnovations.ftpclient;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.FileSystem;
import org.mockftpserver.fake.filesystem.Permissions;
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;

public class FtpClientTest {

    private static final String SERVER_HOST = "localhost";
    private static final int SERVER_PORT = 10000;
    private static final String USER_PASSWORD = "test";
    private static final String USER_NAME = "user";
    private static final String PATH_TO_FOLDER = "/data";
    private static final String FILE_NAME = "filename";

    private FakeFtpServer mFtpServer;

    @Rule
    public TemporaryFolder mTemporaryFolder = new TemporaryFolder();

    @Before
    public void setUp() throws Exception {
        FileSystem fileSystem = new UnixFakeFileSystem();
        DirectoryEntry directoryEntry = new DirectoryEntry(PATH_TO_FOLDER);
        directoryEntry.setPermissions(new Permissions("rwxrwxrw-"));
        FileEntry fileEntry = new FileEntry(PATH_TO_FOLDER + "/" + FILE_NAME);
        fileSystem.add(directoryEntry);
        fileSystem.add(fileEntry);
        UserAccount account = new UserAccount(USER_NAME, USER_PASSWORD, "/");
        mFtpServer = new FakeFtpServer();
        mFtpServer.setFileSystem(fileSystem);
        mFtpServer.addUserAccount(account);
        mFtpServer.setServerControlPort(SERVER_PORT);
        mFtpServer.start();
    }

    @After
    public void tearDown() throws Exception {
        mFtpServer.stop();
    }

    @Test
    public void testCorrect() throws Exception {
        FtpClient ftpClient = new FtpClient(SERVER_HOST, SERVER_PORT, USER_NAME, USER_PASSWORD);
        ftpClient.setFolderOnServer(PATH_TO_FOLDER);
        ftpClient.setFileNameOnServer(FILE_NAME);

        InputStream testData = IOUtils.toInputStream("TestString", Charset.defaultCharset());
        boolean result = ftpClient.upload(testData);
        Assert.assertTrue(result);
    }

    @Test
    public void testIncorrectLogin() throws Exception {
        FtpClient ftpClient = new FtpClient(SERVER_HOST, SERVER_PORT, "", USER_PASSWORD);
        ftpClient.setFolderOnServer(PATH_TO_FOLDER);
        ftpClient.setFileNameOnServer(FILE_NAME);

        InputStream testData = IOUtils.toInputStream("TestString", Charset.defaultCharset());
        boolean result = ftpClient.upload(testData);
        Assert.assertFalse(result);
    }

    @Test
    @Ignore
    public void testIncorrectInputStream() throws Exception {
        FtpClient ftpClient = new FtpClient(SERVER_HOST, SERVER_PORT, USER_NAME, USER_PASSWORD);
        ftpClient.setFolderOnServer(PATH_TO_FOLDER);
        ftpClient.setFileNameOnServer(FILE_NAME);

        boolean result1 = ftpClient.upload(null);
        Assert.assertFalse(result1);
    }

    @Test
    public void testCorrectDownload() throws Exception {
        FtpClient ftpClient = new FtpClient(SERVER_HOST, SERVER_PORT, USER_NAME, USER_PASSWORD);
        ftpClient.setFolderOnServer(PATH_TO_FOLDER);
        ftpClient.setFileNameOnServer(FILE_NAME);

        String testString = "TestString";
        InputStream testData = IOUtils.toInputStream(testString, Charset.defaultCharset());
        ftpClient.upload(testData);

        String localFileName = "filename";
        File tempFolder = mTemporaryFolder.newFolder("path", "to");
        File uploaded = ftpClient.download(String.format("%s/%s", tempFolder, localFileName));
        Assert.assertTrue(uploaded.exists());
    }

    @Test(expected = Exception.class)
    public void testFileNotExistsOnServer() throws Exception {
        FtpClient ftpClient = new FtpClient(SERVER_HOST, SERVER_PORT, USER_NAME, USER_PASSWORD);
        ftpClient.setFolderOnServer(PATH_TO_FOLDER);
        ftpClient.setFileNameOnServer(FILE_NAME);

        String localFileName = "filename";
        File tempFolder = mTemporaryFolder.newFolder("path", "to");
        ftpClient.download(String.format("%s/%s", tempFolder, localFileName));
    }

}