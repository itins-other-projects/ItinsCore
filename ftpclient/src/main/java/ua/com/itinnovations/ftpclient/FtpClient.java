package ua.com.itinnovations.ftpclient;

import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class FtpClient {

    private String mHost;
    private int mPort;
    private String mLogin;
    private String mPassword;
    private String mFolderOnServer;
    private String mFileNameOnServer;

    private FTPClient mFtpClient;

    private FtpClient() {
    }

    public FtpClient(String host, int port, String login, String password) {
        this();
        mHost = host;
        mPort = port;
        mLogin = login;
        mPassword = password;
    }

    public void setFolderOnServer(String folderOnServer) {
        mFolderOnServer = folderOnServer;
    }

    public void setFileNameOnServer(String fileNameOnServer) {
        mFileNameOnServer = fileNameOnServer;
    }

    public boolean upload(InputStream is) {
        boolean result = false;
        try {
            mFtpClient = connectToFtpServer();
            mFtpClient.changeWorkingDirectory(mFolderOnServer);
            result = mFtpClient.storeFile(mFileNameOnServer, is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            disconnectFromFtpServer();
        }
        return result;
    }

    public File download(String destination) throws IOException {
        File outputFile = new File(destination);
        try {
            mFtpClient = connectToFtpServer();
            mFtpClient.changeWorkingDirectory(mFolderOnServer);
            InputStream is = mFtpClient.retrieveFileStream(mFileNameOnServer);
            FileUtils.copyInputStreamToFile(is, outputFile);
        } finally {
            disconnectFromFtpServer();
        }
        return outputFile;
    }

    private FTPClient connectToFtpServer() throws IOException {
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect(mHost, mPort);
        ftpClient.login(mLogin, mPassword);
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        return ftpClient;
    }

    private void disconnectFromFtpServer() {
        try {
            mFtpClient.logout();
            mFtpClient.disconnect();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}
