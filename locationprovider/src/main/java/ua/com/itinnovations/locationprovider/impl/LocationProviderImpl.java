package ua.com.itinnovations.locationprovider.impl;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import ua.com.itinnovations.locationprovider.base.AbstractLocationProvider;

public class LocationProviderImpl extends AbstractLocationProvider {

    private static final long LOCATION_UPDATE_INTERVAL = 1000L;

    private GoogleApiClient mGoogleApiClient;

    public LocationProviderImpl() {
        super();
    }

    @Override
    public void requestLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, getRequest(), this);
    }

    @Override
    public void setGoogleApiClient(GoogleApiClient client) {
        mGoogleApiClient = client;
    }

    @Override
    public void destroy() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private LocationRequest getRequest() {
        return new LocationRequest()
                .setInterval(LOCATION_UPDATE_INTERVAL)
                .setFastestInterval(LOCATION_UPDATE_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

}
