package ua.com.itinnovations.locationprovider.impl;

import android.location.Location;

import ua.com.itinnovations.bluetooth.bluetoothclient.BluetoothClient;
import ua.com.itinnovations.locationprovider.BluetoothLocationProvider;
import ua.com.itinnovations.locationprovider.base.AbstractLocationProvider;
import ua.com.itinnovations.logger.Logger;

public class BluetoothLocationProviderImpl extends AbstractLocationProvider implements BluetoothLocationProvider {

    private BluetoothClient mBluetoothClient;
    private Logger mLogger;

    public BluetoothLocationProviderImpl() {
    }

    public BluetoothLocationProviderImpl(BluetoothClient bluetoothClient, Logger logger) {
        mBluetoothClient = bluetoothClient;
        mLogger = logger;
    }

    @Override
    public void requestLocationUpdates() {
        mBluetoothClient.setDeviceDataListener(this::handleNewDataFromAntenna);
    }

    private void handleNewDataFromAntenna(String data) {
        try {
            Location location = parse(data);
            onLocationChanged(location);
        } catch (Exception e) {
            String logMessage = String.format("Unable to parse \"%s\" %s", data, e.getMessage());
            mLogger.wtf("BluetoothLocationProviderImpl", logMessage, e);
        }
    }

    private Location parse(String data) {
        Location result = new Location("NMEA");
        String[] messages = data.split("\r\n");
        for (String line : messages) {
            if (line.startsWith("$GPVTG")) {
                String[] locationData = line.split(",");
                if (locationData.length > 7) {
                    String bearing = locationData[1];
                    if (!bearing.equals("")) {
                        result.setBearing(Float.parseFloat(bearing));
                    }
                    String speed = locationData[7];
                    if (!speed.equals("")) {
                        result.setSpeed(Float.parseFloat(speed));
                    }
                }
            }
            if (line.startsWith("$GPRMC")) {
                String[] locationData = line.split(",");
                if (locationData.length > 12) {
                    String bearing = locationData[8];
                    if (!bearing.equals("")) {
                        result.setBearing(Float.parseFloat(bearing));
                    }
                    String speed = locationData[7];
                    if (!speed.equals("")) {
                        result.setSpeed(Float.parseFloat(speed) * 0.514444F);
                    }

                    String tmp = locationData[3];
                    String latitude = "";
                    if (tmp.length() > 5) {
                        int deg = Integer.parseInt(tmp.substring(0, 2));
                        double min = Double.parseDouble(tmp.substring(2));
                        latitude = (deg + (min / 60)) + "";
                    }

                    tmp = locationData[5];
                    String longitude = "";
                    if (tmp.length() > 5) {
                        int deg = Integer.parseInt(tmp.substring(0, 3));
                        double min = Double.parseDouble(tmp.substring(3));
                        longitude = (deg + (min / 60)) + "";
                    }

                    result.setLatitude(Double.parseDouble(latitude.isEmpty() ? "0" : latitude));
                    result.setLongitude(Double.parseDouble(longitude.isEmpty() ? "0" : longitude));
                }
            }
            if (line.startsWith("$GPGGA")) {
                String[] locationData = line.split(",");
                if (locationData.length > 12) {
                    String tmp = locationData[2];
                    String latitude = "";
                    if (tmp.length() > 5) {
                        int deg = Integer.parseInt(tmp.substring(0, 2));
                        double min = Double.parseDouble(tmp.substring(2));
                        latitude = (deg + (min / 60)) + "";
                    }

                    tmp = locationData[4];
                    String longitude = "";
                    if (tmp.length() > 5) {
                        int deg = Integer.parseInt(tmp.substring(0, 3));
                        double min = Double.parseDouble(tmp.substring(3));
                        longitude = (deg + (min / 60)) + "";
                    }

                    String altitude = locationData[9];

                    result.setLatitude(Double.parseDouble(latitude.isEmpty() ? "0" : latitude));
                    result.setLongitude(Double.parseDouble(longitude.isEmpty() ? "0" : longitude));
                    result.setAltitude(Double.parseDouble(altitude.isEmpty() ? "0" : altitude));
                }
            }
        }
        result.setAccuracy(5);
        return (0.0 != result.getLatitude()) ? result : null;
    }

}
