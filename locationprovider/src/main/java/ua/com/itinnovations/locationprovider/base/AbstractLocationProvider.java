package ua.com.itinnovations.locationprovider.base;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

import ua.com.itinnovations.locationprovider.LocationProvider;

public abstract class AbstractLocationProvider implements LocationProvider {

    private List<LocationListener> mSubjects;

    public AbstractLocationProvider() {
        mSubjects = new ArrayList<>();
    }

    @Override
    public void subscribe(LocationListener consumer) {
        mSubjects.add(consumer);
    }

    @Override
    public void onLocationChanged(Location location) {
        for (LocationListener locationListener: mSubjects) {
            locationListener.onLocationChanged(location);
        }
    }

}
