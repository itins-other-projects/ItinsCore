package ua.com.itinnovations.locationprovider;

import com.google.android.gms.common.api.GoogleApiClient;

public interface BluetoothLocationProvider extends LocationProvider {

    @Override
    default void setGoogleApiClient(GoogleApiClient client) {
    }

    @Override
    default void destroy() {
    }

}
