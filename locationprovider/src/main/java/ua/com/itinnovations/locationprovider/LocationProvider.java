package ua.com.itinnovations.locationprovider;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;

public interface LocationProvider extends LocationListener {

    void subscribe(LocationListener consumer);

    void requestLocationUpdates();

    void setGoogleApiClient(GoogleApiClient client);

    void destroy();


    @FunctionalInterface
    interface LocationListener {

        void onLocationChanged(Location location);

    }

}
