package ua.com.itinnovations.deviceinfo;

/**
 * This interface describes device status receiver, for storing data about state of device
 * such as battery level, external power supply, GSM etc.
 * Provider must implement this interface and returns {@link DeviceInfo} instance with
 * data, which it can provides. F.ex., battery broadcast receiver will provide data about
 * battery level and external power supply, so it will put this values to {@link DeviceInfo}.
 */
public interface DeviceInfoProvider {

    /**
     * Requests data about state of the device. This data will be returned as {@link DeviceInfo}.
     *
     * @return POJO with data from one of several device monitors (f.ex. GSM, battery).
     */
    DeviceInfo requestDeviceInfo();

}
