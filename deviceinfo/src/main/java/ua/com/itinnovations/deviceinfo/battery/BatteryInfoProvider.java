package ua.com.itinnovations.deviceinfo.battery;

import ua.com.itinnovations.deviceinfo.DeviceInfoProvider;

public interface BatteryInfoProvider extends DeviceInfoProvider {

    void addBatteryInfoListener(BatteryInfoListener listener);

    void scheduleBatteryInfoUpdates(long batteryRequestTimeout);

    interface BatteryInfoListener {

        void onBatteryInfoReceived(BatteryInfo batteryInfo);

    }

}
