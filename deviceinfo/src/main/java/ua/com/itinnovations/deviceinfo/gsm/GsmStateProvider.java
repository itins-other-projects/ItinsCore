package ua.com.itinnovations.deviceinfo.gsm;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import ua.com.itinnovations.deviceinfo.DeviceInfo;
import ua.com.itinnovations.deviceinfo.DeviceInfoProvider;

public class GsmStateProvider implements DeviceInfoProvider {

    public static final int STRENGTH_UNKNOWN = 99;

    private int mGsmStrength;

    public GsmStateProvider(final Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        mGsmStrength = STRENGTH_UNKNOWN;
        telephonyManager.listen(getPhoneStateListener(), PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    @Override
    public DeviceInfo requestDeviceInfo() {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setGsmStrength(mGsmStrength);
        return deviceInfo;
    }

    private PhoneStateListener getPhoneStateListener() {
        return new PhoneStateListener() {
            @Override
            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                parseReceivedStrength(signalStrength);
            }
        };
    }

    private void parseReceivedStrength(SignalStrength signalStrength) {
        mGsmStrength = signalStrength.getGsmSignalStrength();
    }

}
