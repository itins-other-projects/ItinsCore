package ua.com.itinnovations.deviceinfo;

import ua.com.itinnovations.deviceinfo.battery.BatteryInfo;

public class DeviceInfo {

    private BatteryInfo mBatteryInfo;
    private int mGsmStrength;

    public DeviceInfo() {
        mBatteryInfo = new BatteryInfo(-1, false, -1);
        mGsmStrength = -1;
    }

    public void setBatteryInfo(BatteryInfo batteryInfo) {
        mBatteryInfo = batteryInfo;
    }

    public void setGsmStrength(int gsmStrength) {
        mGsmStrength = gsmStrength;
    }

    public BatteryInfo getBatteryInfo() {
        return mBatteryInfo;
    }

    public int getGsmStrength() {
        return mGsmStrength;
    }
}
