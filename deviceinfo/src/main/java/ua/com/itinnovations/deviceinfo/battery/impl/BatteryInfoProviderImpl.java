package ua.com.itinnovations.deviceinfo.battery.impl;

import ua.com.itinnovations.deviceinfo.DeviceInfo;
import ua.com.itinnovations.deviceinfo.battery.BatteryInfoProvider;

class BatteryInfoProviderImpl implements BatteryInfoProvider {

    @Override
    public DeviceInfo requestDeviceInfo() {
        return null;
    }

    @Override
    public void addBatteryInfoListener(BatteryInfoListener listener) {

    }

    @Override
    public void scheduleBatteryInfoUpdates(long batteryRequestTimeout) {

    }
}
