package ua.com.itinnovations.deviceinfo.battery;

public class BatteryInfo {

    private int mBatteryLevel;
    private boolean mCharging;
    private int mVoltage;

    public BatteryInfo(int batteryLevel, boolean charging, int voltage) {
        mBatteryLevel = batteryLevel;
        mCharging = charging;
        mVoltage = voltage;
    }

    public int getBatteryLevel() {
        return mBatteryLevel;
    }

    public boolean isCharging() {
        return mCharging;
    }

    public int getVoltage() {
        return mVoltage;
    }

}
