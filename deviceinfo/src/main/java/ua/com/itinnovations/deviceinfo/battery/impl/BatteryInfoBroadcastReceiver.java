package ua.com.itinnovations.deviceinfo.battery.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import ua.com.itinnovations.deviceinfo.DeviceInfo;
import ua.com.itinnovations.deviceinfo.battery.BatteryInfo;
import ua.com.itinnovations.deviceinfo.battery.BatteryInfoProvider;

public class BatteryInfoBroadcastReceiver extends BroadcastReceiver implements BatteryInfoProvider {

    private static final int DEFAULT_BATTERY_VALUE = -1;
    private static final int DEFAULT_VOLTAGE = 0;

    private static final long BATTERY_STATE_REQUEST_TIMEOUT = 1000 * 10 * 60;

    private Context mContext;
    private Handler mBatteryInfoRequestHandler;
    private int mLastBatteryValue;
    private long mBatteryRequestTimeout;
    private List<BatteryInfoListener> mBatteryInfoListeners;

    public BatteryInfoBroadcastReceiver(Context context) {
        mContext = context;
        mLastBatteryValue = DEFAULT_BATTERY_VALUE;
        mBatteryRequestTimeout = BATTERY_STATE_REQUEST_TIMEOUT;
        mBatteryInfoListeners = new ArrayList<>();
        mBatteryInfoRequestHandler = new Handler();
    }

    @Override
    public void scheduleBatteryInfoUpdates(long batteryRequestTimeout) {
        mBatteryRequestTimeout = batteryRequestTimeout;
        mBatteryInfoRequestHandler.post(this::requestBatteryState);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (null != intent) {
            notifyConsumers(parseResponseFromBatteryBroadcast(intent));
        }
    }

    @Override
    public DeviceInfo requestDeviceInfo() {
        Intent intent = mContext.registerReceiver(this, getBatteryIntentFilter());
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setBatteryInfo(parseResponseFromBatteryBroadcast(intent));
        return deviceInfo;
    }

    @Override
    public void addBatteryInfoListener(BatteryInfoListener listener) {
        mBatteryInfoListeners.add(listener);
    }

    private void requestBatteryState() {
        mContext.registerReceiver(this, getBatteryIntentFilter());
        mBatteryInfoRequestHandler.postDelayed(this::requestBatteryState, mBatteryRequestTimeout);
    }

    private BatteryInfo parseResponseFromBatteryBroadcast(Intent intent) {
        return (null != intent) ? getBatteryInfo(intent) : getStubbedBatteryInfo();
    }

    private BatteryInfo getStubbedBatteryInfo() {
        return new BatteryInfo(DEFAULT_BATTERY_VALUE, false, DEFAULT_VOLTAGE);
    }

    private BatteryInfo getBatteryInfo(Intent intent) {
        int batteryLevel = getBatteryLevel(intent);
        boolean isCharging = isCharging(intent);
        int voltage = getVoltageValue(intent);
        mLastBatteryValue = batteryLevel;
        return new BatteryInfo(mLastBatteryValue, isCharging, voltage);
    }

    private int getBatteryLevel(Intent intent) {
        return intent.getIntExtra(BatteryManager.EXTRA_LEVEL, mLastBatteryValue);
    }

    private boolean isCharging(Intent intent) {
        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, DEFAULT_BATTERY_VALUE);
        boolean isCharging = BatteryManager.BATTERY_STATUS_CHARGING == status;
        boolean isCharged = BatteryManager.BATTERY_STATUS_FULL == status;
        return isCharging || isCharged;
    }

    private int getVoltageValue(Intent intent) {
        return intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, DEFAULT_VOLTAGE);
    }

    private void notifyConsumers(BatteryInfo batteryInfo) {
        for (BatteryInfoListener listener: mBatteryInfoListeners) {
            listener.onBatteryInfoReceived(batteryInfo);
        }
    }

    private IntentFilter getBatteryIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        filter.addAction(Intent.ACTION_POWER_CONNECTED);
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        return filter;
    }

}
