package ua.com.itinnovations.wialonclient.client.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import ua.com.itinnovations.socketclient.SocketClient;
import ua.com.itinnovations.socketclient.SocketClient.SocketClientStateListener;
import ua.com.itinnovations.socketclient.SocketClient.SocketDataReceivedListener;
import ua.com.itinnovations.socketclient.SocketClient.SocketState;
import ua.com.itinnovations.wialonclient.client.WialonClient;
import ua.com.itinnovations.wialonclient.packages.MessagePackage;
import ua.com.itinnovations.wialonclient.packages.Package;
import ua.com.itinnovations.wialonclient.packages.request.BlackBoxPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.LoginPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.PingPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.ServerMessagePackageRequest;
import ua.com.itinnovations.wialonclient.packages.response.BlackBoxPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.DataPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.LoginPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.MessagePackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.PingPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.ServerMessagePackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.ShortDataPackageResponse;

/**
 * Non cached implementation of {@link WialonClient}. This impl, only sends packages to server
 * and ping it, if it necessary. Ping is configurable feature and it possible to enable and
 * disable it.
 */
public class WialonClientImpl implements WialonClient {

    private static final int MILLIS_PER_SECOND = 1000;

    /**
     * Socket, via which data transferred to server using wialon (IPS) protocol
     */
    private SocketClient mSocketClient;
    /**
     * Interval, with which client should ping server. It must be bigger then one second,
     * otherwise #keepSocketAlive will skip calling and do not set auto ping mode.
     */
    private int mPingInterval;
    /**
     * Time, when was sent last package in millis. This time is necessary for auto-ping mode,
     * when client by special timeout checks, is necessary to ping server. This field updates,
     * when new messages sends
     */
    private long mLastSendTime;
    /**
     * If is impossible to connect to server, f.ex. by license, this flag will be true, otherwise
     * it will be false
     */
    private boolean mStubbed;
    /**
     * This map necessary for storing listeners of data from server. <br />
     * Read more about default listeners in doc to {@link WialonClient}
     */
    private Map<String, List<WialonClientListener>> mConsumers;

    /**
     * Default constructor. It should be called, when connection to server is impossible
     * f.ex. via license
     */
    public WialonClientImpl() {
        mStubbed = true;
    }

    /**
     * Usual constructor, which should called, for creating standard {@link WialonClient} for
     * interacting with server. <br />
     * This constructor should be called, if, and only if, you will manage socket-connection
     * by yourself. Otherwise, if you want, that this class manage connection, you should to
     * use constructor below
     *
     * @param socketClient {@link SocketClient} implementation, via which will transfer data
     *                     from client to server and from server to client
     */
    public WialonClientImpl(SocketClient socketClient) {
        mStubbed = false;
        mSocketClient = socketClient;
        mSocketClient.setConsumer(getSocketReceivedDataConsumer());
        mConsumers = initConsumersMap();
    }

    /**
     * Usual constructor, which should called, for creating standard {@link WialonClient} for
     * interacting with server
     *
     * @param socketClient {@link SocketClient} implementation, via which will transfer data
     *                     from client to server and from server to client
     * @param login string with login on server. For android devices this will be imei of device
     * @param password string with password for authorisation on server
     */
    public WialonClientImpl(SocketClient socketClient, String login, String password) {
        this(socketClient);
        mSocketClient.setSocketClientStateListener(getSocketClientStateListener(login, password));
        mSocketClient.connect();
    }

    /**
     * Add new receiver to collection, with name from keys (see details by next links)<br />
     * Key for Tracker responses: {@link WialonClient#TRACKER_CONSUMER_NAME} <br />
     * Key for messages and commands: {@link WialonClient#MESSAGE_CONSUMER_NAME} <br />
     * Key for login responses: {@link WialonClient#CONNECTION_STATE_CONSUMER_NAME} <br />
     * Key for pings: {@link WialonClient#PING_CONSUMER_NAME} <br />
     *
     * @param receiverName this value takes from set of default receivers' names.
     *                     (see above for details)
     * @param consumer and instance of new consumer, which will called, when an event will happen<br />
     *                 (see more in doc to {@link WialonClientListener}
     */
    @Override
    public void addReceiver(String receiverName, WialonClientListener consumer) {
        List<WialonClientListener> consumers = mConsumers.get(receiverName);
        consumers.add(consumer);
        mConsumers.put(receiverName, consumers);
    }

    /**
     * @return return true, if client can't be connected to server, because it's impossible by license,
     * else return false
     */
    @Override
    public boolean isStubbed() {
        return mStubbed;
    }

    /**
     * @return true, if client connected to server and it's possible to send something or receive.
     * Otherwise, returns false
     */
    @Override
    public boolean isConnectedToServer() {
        return (null != mSocketClient) && mSocketClient.isConnected();
    }

    /**
     * Method for sending login package to server. This method is just useful wrapper around
     * {@link #send} method for sending login-package
     *
     * @param login, String with login on server (for mobile devices, it will uses imei-address)
     * @param password, String with password on server
     */
    @Override
    public void login(String login, String password) {
        send(new LoginPackageRequest(login, password));
    }

    /**
     * Sends package to server. Will transform an {@link Package} object to string and sends it
     * to socket stream.<br />
     * See details about {@link Package} and its successors:<br />
     * <ul>
     *     <li>{@link MessagePackage} and its successors are necessary for sending and receiving
     *     messages.</li>
     *     <li>{@link BlackBoxPackageRequest} as most popular package which sends to server.
     *     This package is necessary for transferring filtered points by tracker.<br /> For reading
     *     more about other packages, you should open package with this classes and read their doc</li>
     *     <li>{@link LoginPackageRequest} package with login information. It sends mostly by special
     *     {@link #login} method</li>
     *     <li>{@link PingPackageRequest}. This package sends automatically, when {@link #keepSocketAlive}
     *     was called with correct params</li>
     * </ul>

     * @param pckg, an instance of class-successor of {@link Package} with info for server.
     *              It can be response for server command/message or request-package with points
     *              from tracker, or just message from client.<br />
     *
     */
    @Override
    public void send(Package pckg) {
        mLastSendTime = System.currentTimeMillis();
        mSocketClient.write(pckg.toString());
    }

    /**
     * When this method called, client will keep socket-connection to server alive always.
     * This method need to be called one time, with {@param pingInterval} > on second in millis.
     * Otherwise, client will not keep socket alive, and just skip this call.
     *
     * @param pingInterval time between sending to server. If timeout between last sending and
     *                     current time is bigger then this value, then client will sends special
     *                     ping-package
     */
    @Override
    public void keepSocketAlive(int pingInterval) {
        mPingInterval = pingInterval * MILLIS_PER_SECOND;
        if (MILLIS_PER_SECOND < mPingInterval) {
            Timer pingTimer = new Timer();
            pingTimer.schedule(getPingTimerTask(), 0, mPingInterval);
        }
    }

    /**
     * Creates default map with listeners of events from server.<br />
     * Read more about default data receivers's keys in doc to {@link WialonClient}
     *
     * @return new hasmap with default empty lists for every consumer.
     */
    private Map<String, List<WialonClientListener>> initConsumersMap() {
        Map<String, List<WialonClientListener>> consumers = new HashMap<>();
        consumers.put(TRACKER_CONSUMER_NAME, new ArrayList<>());
        consumers.put(MESSAGE_CONSUMER_NAME, new ArrayList<>());
        consumers.put(CONNECTION_STATE_CONSUMER_NAME, new ArrayList<>());
        consumers.put(PING_CONSUMER_NAME, new ArrayList<>());
        return consumers;
    }

    /**
     * Creates socket state listener, where will called login method. This method creates a
     * lambda, which will be called all times, when socket state changes.
     *
     * @param login login string with imei of device
     * @param password password string
     * @return lambda of type {@link SocketClientStateListener}, which will call, when socket
     * connected
     */
    private SocketClientStateListener getSocketClientStateListener(String login, String password) {
        return socketState -> handleSocketState(login, password, socketState);
    }

    /**
     * Handle changing socket state. There're two states of {@link SocketClient}:<br />
     * <ul>
     *     <li>{@link SocketState#CONNECTED}, which is returned, when socket normally created</li>
     *     <li>{@link SocketState#DISCONNECTED}, which is returned, when socket is broken or not
     *     created</li>
     * </ul>
     *
     * @param login string with imei of device
     * @param password password string
     * @param socketState state of {@link SocketClient}
     */
    private void handleSocketState(String login, String password, SocketState socketState) {
        if (SocketState.CONNECTED == socketState) {
            login(login, password);
        }
    }

    /**
     * Ping task, which check, is time to ping server, and ping, if true. Also, schedule
     * new timeout for next checking
     */
    private TimerTask getPingTimerTask() {
        return new TimerTask() {
            @Override
            public void run() {
                if (isTimeToPingServer()) send(new PingPackageRequest());
            }
        };
    }

    /**
     * Check, is time to ping server. It is true, when from last sending was spent time
     * more then {@link #mPingInterval}
     * @return true, if time to ping server, false otherwise
     */
    private boolean isTimeToPingServer() {
        return (System.currentTimeMillis() - mLastSendTime) > mPingInterval;
    }

    /**
     * Socket data receiver. It will be called, when {@link SocketClient} read from it input stream
     * some value
     *
     * @return instance of {@link SocketDataReceivedListener}, which will called by {@link SocketClient},
     * for transferring data to this class
     */
    private SocketDataReceivedListener getSocketReceivedDataConsumer() {
        return s -> callClientConsumerForReceivedPackage(Package.parseWialonString(s));
    }

    /**
     * Check type of received package and select special consumer name from default.
     * See more about default consumers in doc for {@link WialonClient}
     *
     * @param pckg received package, which is necessary to transfer to consumer
     */
    private void callClientConsumerForReceivedPackage(Package pckg) {
        if (pckg instanceof LoginPackageResponse) {
            notifyConsumers(CONNECTION_STATE_CONSUMER_NAME, pckg);
        } else if (pckg instanceof PingPackageResponse) {
            notifyConsumers(PING_CONSUMER_NAME, pckg);
        } else if (pckg instanceof DataPackageResponse) {
            notifyConsumers(TRACKER_CONSUMER_NAME, pckg);
        } else if (pckg instanceof ShortDataPackageResponse) {
            notifyConsumers(TRACKER_CONSUMER_NAME, pckg);
        } else if (pckg instanceof BlackBoxPackageResponse) {
            notifyConsumers(TRACKER_CONSUMER_NAME, pckg);
        } else if (pckg instanceof MessagePackageResponse) {
            notifyConsumers(MESSAGE_CONSUMER_NAME, pckg);
        } else if (pckg instanceof ServerMessagePackageRequest) {
            notifyConsumers(MESSAGE_CONSUMER_NAME, pckg);
        } else if (pckg instanceof ServerMessagePackageResponse) {
            notifyConsumers(MESSAGE_CONSUMER_NAME, pckg);
        }
    }

    /**
     * Get consumers from {@link #mConsumers} by {@param consumerName} and notify about data received
     * for them.
     *
     * @param consumerName one from default consumers' names. See {@link WialonClient} doc for details
     * @param pckg received package, which necessary to transfer to consumer
     */
    private void notifyConsumers(String consumerName, Package pckg) {
        List<WialonClientListener> listeners = mConsumers.get(consumerName);
        if (null != listeners) {
            for (WialonClientListener listener : listeners) listener.consume(pckg);
        }
    }

}
