package ua.com.itinnovations.wialonclient.client;

import ua.com.itinnovations.wialonclient.packages.MessagePackage;
import ua.com.itinnovations.wialonclient.packages.Package;
import ua.com.itinnovations.wialonclient.packages.request.BlackBoxPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.LoginPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.PingPackageRequest;

/**
 * Client, for Wialon (IPS) protocol, which is necessary to interact to IPS-server,
 * sending and receiving data between client and server.
 */
public interface WialonClient {

    /**
     * Key name of receivers of events from server, considered with Tracker. This listeners
     * will called, when from server arrived package specially for Tracker.
     *
     * For Tracker its possible to handle only responses about sending points to server.
     */
    String TRACKER_CONSUMER_NAME = "TRACKER";
    /**
     * This key is used by receivers of messages (or commands) from server, which are
     * appear from server. This receivers can handle requests from server and responses,
     * which are sent by server for messages from client
     */
    String MESSAGE_CONSUMER_NAME = "MESSAGE";
    /**
     * This key uses by receivers of connection server state. This receivers will receive only
     * responses for login packages
     */
    String CONNECTION_STATE_CONSUMER_NAME = "SERVER_STATE";
    /**
     * This receivers' key used for handle response to pings. This mechanism used for keeping
     * connection with server
     */
    String PING_CONSUMER_NAME = "PING";

    /**
     * Add new receiver to collection, with name from keys (see details by next links)<br />
     * Key for Tracker responses: {@link WialonClient#TRACKER_CONSUMER_NAME} <br />
     * Key for messages and commands: {@link WialonClient#MESSAGE_CONSUMER_NAME} <br />
     * Key for login responses: {@link WialonClient#CONNECTION_STATE_CONSUMER_NAME} <br />
     * Key for pings: {@link WialonClient#PING_CONSUMER_NAME} <br />
     *
     * @param receiverName this value takes from set of default receivers' names.
     *                     (see above for details)
     * @param consumer and instance of new consumer, which will called, when an event will happen<br />
     *                 (see more in doc to {@link WialonClientListener}
     */
    void addReceiver(String receiverName, WialonClientListener consumer);

    /**
     * @return return true, if client can't be connected to server, because it's impossible by license,
     * else return false
     */
    boolean isStubbed();

    /**
     * @return true, if client connected to server and it's possible to send something or receive. Otherwise,
     * returns false
     */
    boolean isConnectedToServer();

    /**
     * Method for sending login package to server. This method is just useful wrapper around
     * {@link #send} method for sending login-package
     *
     * @param login, String with login on server (for mobile devices, it will uses imei-address)
     * @param password, String with password on server
     */
    void login(String login, String password);

    /**
     * Sends package to server. Will transform an {@link Package} object to string and sends it
     * to socket stream.<br />
     * See details about {@link Package} and its successors:<br />
     * <ul>
     *     <li>{@link MessagePackage} and its successors are necessary for sending and receiving
     *     messages.</li>
     *     <li>{@link BlackBoxPackageRequest} as most popular package which sends to server.
     *     This package is necessary for transferring filtered points by tracker.<br /> For reading
     *     more about other packages, you should open package with this classes and read their doc</li>
     *     <li>{@link LoginPackageRequest} package with login information. It sends mostly by special
     *     {@link #login} method</li>
     *     <li>{@link PingPackageRequest}. This package sends automatically, when {@link #keepSocketAlive}
     *     was called with correct params</li>
     * </ul>

     * @param pckg, an instance of class-successor of {@link Package} with info for server.
     *              It can be response for server command/message or request-package with points
     *              from tracker, or just message from client.<br />
     *
     */
    void send(Package pckg);

    /**
     * When this method called, client will keep socket-connection to server alive always.
     * This method need to be called one time, with {@param pingInterval} > on second in millis.
     * Otherwise, client will not keep socket alive, and just skip this call.
     *
     * @param pingInterval time between sending to server. If timeout between last sending and
     *                     current time is bigger then this value, then client will sends special
     *                     ping-package
     */
    void keepSocketAlive(int pingInterval);


    /**
     * Functional interface, which uses as callback, for events from server (responses,
     * commands, or messages from it).
     * When an event like it appears, client will find necessary list with listeners,
     * iterate it and notify all listeners about an event happen
     */
    @FunctionalInterface
    interface WialonClientListener {

        /**
         * Listener, which will called, when an event from server appear.
         *
         * @param pckg a package, which received from server. It can be just response for
         *             sent before package or it can be message or command from server
         */
        void consume(Package pckg);

    }

}
