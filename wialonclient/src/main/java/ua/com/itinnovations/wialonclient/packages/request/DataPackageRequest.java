package ua.com.itinnovations.wialonclient.packages.request;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class DataPackageRequest extends ShortDataPackageRequest {

    private float mHDop;
    private int mInputs;
    private int mOutputs;
    private String mAdc;
    private String mIButton;
    private List<Param> mParams;

    public DataPackageRequest(Calendar dateTime,
                              double latitude,
                              double longitude,
                              int speed,
                              int course,
                              int altitude,
                              int satellites,
                              float hDop,
                              int inputs,
                              int outputs,
                              String adc,
                              String iButton,
                              List<Param> params
    ) {
        super(dateTime, latitude, longitude, speed, course, altitude, satellites);
        mHDop = hDop;
        mInputs = inputs;
        mOutputs = outputs;
        mAdc = adc;
        mIButton = iButton;
        mParams = params;
    }

    public DataPackageRequest(Calendar dateTime,
                              double latitude,
                              double longitude,
                              int speed,
                              int course,
                              int altitude,
                              int satellites,
                              float hDop,
                              int inputs,
                              int outputs,
                              String adc,
                              String iButton
    ) {
        super(dateTime, latitude, longitude, speed, course, altitude, satellites);
        mHDop = hDop;
        mInputs = inputs;
        mOutputs = outputs;
        mAdc = adc;
        mIButton = iButton;
        mParams = new ArrayList<>();
    }

    public DataPackageRequest(Calendar dateTime,
                              double latitude,
                              double longitude,
                              int speed,
                              int course,
                              int altitude,
                              int satellites
    ) {
        super(dateTime, latitude, longitude, speed, course, altitude, satellites);
        mHDop = 0.0F;
        mInputs = 0;
        mOutputs = 0;
        mAdc = "";
        mIButton = "NA";
        mParams = new ArrayList<>();
    }

    @Override
    String toStringWithoutHeader() {
        return String.format(
                Locale.getDefault(),
                "%s;%s;%s;%s;%s;%s;%s",
                super.toStringWithoutHeader(),
                getHDop(),
                getInputs(),
                getOutputs(),
                mAdc,
                mIButton,
                getParams()
        );
    }

    private String getParams() {
        StringBuilder builder = new StringBuilder();
        for (Param param: mParams) {
            builder.append(param.toString());
        }
        String result = builder.toString();
        return !Objects.equals(result, "") ? result.substring(0, result.length() - 1) : "";
    }

    private String getOutputs() {
        return (0 == mOutputs) ? "NA" : String.valueOf(mOutputs);
    }

    private String getInputs() {
        return (0 == mInputs) ? "NA" : String.valueOf(mInputs);
    }

    private String getHDop() {
        return (0.0F == mHDop) ? "NA" : String.valueOf(mHDop);
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "#D#%s;%s", toStringWithoutHeader(), END_OF_LINE);
    }

    public static class Param {

        private String mKey;
        private int mType;
        private String mValue;

        public Param(String key, int type, String value) {
            mKey = key;
            mType = type;
            mValue = value;
        }

        @Override
        public String toString() {
            return String.format(Locale.getDefault(), "%s:%d:%s,", mKey, mType, mValue);
        }

    }

}
