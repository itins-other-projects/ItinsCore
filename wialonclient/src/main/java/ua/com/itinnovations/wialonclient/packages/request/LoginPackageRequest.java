package ua.com.itinnovations.wialonclient.packages.request;

import ua.com.itinnovations.wialonclient.packages.Package;

public class LoginPackageRequest extends Package {

    private String mLogin;
    private String mPassword;

    public LoginPackageRequest(String login, String password) {
        mLogin = login;
        mPassword = password;
    }

    @Override
    public String toString() {
        return String.format("#L#%s;%s%s", mLogin, mPassword, END_OF_LINE);
    }

}
