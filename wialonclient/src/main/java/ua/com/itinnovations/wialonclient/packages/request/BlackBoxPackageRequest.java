package ua.com.itinnovations.wialonclient.packages.request;

import java.util.List;

import ua.com.itinnovations.wialonclient.packages.Package;

public class BlackBoxPackageRequest<T extends ShortDataPackageRequest> extends Package {

    private static final String DELIMITER = "|";
    private List<T> mDatas;

    public BlackBoxPackageRequest(List<T> datas) {
        mDatas = datas;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("#B#");
        for (ShortDataPackageRequest shortData: mDatas) {
            builder.append(shortData.toStringWithoutHeader()).append(DELIMITER);
        }
        String str = builder.toString();
        return str.substring(0, str.length() - 1).concat(END_OF_LINE);
    }

}
