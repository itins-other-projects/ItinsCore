package ua.com.itinnovations.wialonclient.packages;

import ua.com.itinnovations.wialonclient.packages.request.ServerMessagePackageRequest;
import ua.com.itinnovations.wialonclient.packages.response.ServerMessagePackageResponse;

public abstract class ServerMessagePackage extends MessagePackage {

    private static final int ID_POSITION = 0;
    private static final int TYPE_POSITION = 1;
    private static final int STATUS_POSITION = 2;
    private static final int COMMAND_POSITION = 2;
    private static final int COMMAND_PARAMS_POSITION = 3;

    static ServerMessagePackage parse(String data) throws Exception {
        String[] dataParts = data.split(SEPARATOR);
        return getMessagePackage(dataParts, PackageType.getRequestType(dataParts[TYPE_POSITION]));
    }

    private static ServerMessagePackage getMessagePackage(String[] dataParts, PackageType type) throws Exception {
        return (PackageType.REQUEST == type) ? getRequestPackage(dataParts, type) : getResponsePackage(dataParts);
    }

    private static ServerMessagePackageResponse getResponsePackage(String[] dataParts) throws Exception {
        return new ServerMessagePackageResponse(dataParts[ID_POSITION],
                                                Status.getStatus(dataParts[STATUS_POSITION]));
    }

    private static ServerMessagePackageRequest getRequestPackage(String[] dataParts,
                                                                 PackageType type) throws Exception {
        return new ServerMessagePackageRequest(dataParts[ID_POSITION],
                                               type,
                                               Command.getCommand(dataParts[COMMAND_POSITION]),
                                               dataParts[COMMAND_PARAMS_POSITION]);
    }

}
