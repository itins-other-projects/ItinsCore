package ua.com.itinnovations.wialonclient.packages.response;

import java.util.Locale;

import ua.com.itinnovations.wialonclient.packages.MessagePackage;

public class ClientMessagePackageResponse extends MessagePackage {

    private String mId;
    private String mStatus;

    public ClientMessagePackageResponse(String id, String status) {
        mId = id;
        mStatus = status;
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "#M#%s;%s;%s%s", mId, RESPONSE_PACKAGE_TYPE, mStatus, END_OF_LINE);
    }

}
