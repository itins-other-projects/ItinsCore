package ua.com.itinnovations.wialonclient.packages.request;

import android.util.Log;

import ua.com.itinnovations.wialonclient.packages.MessagePackage;

public class ClientMessagePackageRequest extends MessagePackage {

    private String mId;
    private PackageType mType;
    private Command mCommand;
    private String mData;

    public ClientMessagePackageRequest(String id,
                                       PackageType type,
                                       Command command,
                                       String data) {
        super();
        mId = id;
        mType = type;
        mCommand = command;
        mData = data;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        String string = builder.append("#M#")
                .append(mId)
                .append(SEPARATOR)
                .append(mType)
                .append(SEPARATOR)
                .append(mCommand)
                .append(SEPARATOR)
                .append(mData)
                .append(END_OF_LINE)
                .toString();
        Log.wtf("ClientMessagePackageRequest", string);
        return string;
    }

}
