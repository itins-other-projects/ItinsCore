package ua.com.itinnovations.wialonclient.packages;

import java.util.HashMap;
import java.util.Map;

public class MessagePackage extends Package {

    private static final Map<String, Command> COMMANDS = getCommands();
    private static final Map<String, Status> STATUSES = getStatuses();
    private static final Map<String, PackageType> PACKAGE_TYPES = getPackageTypes();

    public static final String MESSAGE_COMMAND = "MSG";
    public static final String TIMETABLE_COMMAND = "TTMODE";
    public static final String ON_ROUTE_STATUS_COMMAND = "OBJECTROUTESTATUS";
    public static final String INTERVAL_COMMAND = "OBJINT";
    public static final String ROUTE_START_COMMAND = "ROUTESTART";
    public static final String ROUTE_SET_COMMAND = "ROUTESET";

    public static final String SUCCESS_STATUS = "OK";
    public static final String FAILED_STATUS = "ERR";

    public static final String REQUEST_PACKAGE_TYPE = "REQ";
    public static final String RESPONSE_PACKAGE_TYPE = "RES";

    private static Map<String, Command> getCommands() {
        Map<String, Command> commands = new HashMap<>();
        commands.put(MESSAGE_COMMAND, Command.MESSAGE);
        commands.put(TIMETABLE_COMMAND, Command.TIMETABLE_MODE);
        commands.put(ON_ROUTE_STATUS_COMMAND, Command.ON_ROUTE_STATUS);
        commands.put(INTERVAL_COMMAND, Command.INTERVAL);
        commands.put(ROUTE_START_COMMAND, Command.ROUTE_START);
        commands.put(ROUTE_SET_COMMAND, Command.ROUTE_SET);
        return commands;
    }

    private static Map<String, Status> getStatuses() {
        Map<String, Status> statuses = new HashMap<>();
        statuses.put(SUCCESS_STATUS, Status.SUCCESS);
        statuses.put(FAILED_STATUS, Status.FAILED);
        return statuses;
    }

    private static Map<String, PackageType> getPackageTypes() {
        Map<String, PackageType> packageTypes = new HashMap<>();
        packageTypes.put(REQUEST_PACKAGE_TYPE, PackageType.REQUEST);
        packageTypes.put(RESPONSE_PACKAGE_TYPE, PackageType.RESPONSE);
        return packageTypes;
    }

    public enum PackageType {

        REQUEST,
        RESPONSE;

        public static PackageType getRequestType(String type) throws Exception {
            if (PACKAGE_TYPES.containsKey(type)) {
                return PACKAGE_TYPES.get(type);
            }
            throw new Exception(String.format("Unexpected type: %s", type));
        }
    }

    public enum Status {

        SUCCESS,
        FAILED;

        public static Status getStatus(String status) throws Exception {
            if (STATUSES.containsKey(status)) {
                return STATUSES.get(status);
            }
            throw new Exception(String.format("Unexpected status: %s", status));
        }

    }

    public enum Command {

        MESSAGE,
        TIMETABLE_MODE,
        INTERVAL,
        ON_ROUTE_STATUS,
        ROUTE_START,
        ROUTE_SET;

        public static Command getCommand(String command) throws Exception {
            if (COMMANDS.containsKey(command)) {
                return COMMANDS.get(command);
            }
            throw new Exception(String.format("Unexpected command: %s", command));
        }

    }

}
