package ua.com.itinnovations.wialonclient.packages;

import ua.com.itinnovations.wialonclient.packages.response.BlackBoxPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.DataPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.LoginPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.MessagePackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.PingPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.ShortDataPackageResponse;

public abstract class Package {

    protected static final String END_OF_LINE = "\r\n";
    protected static final String SEPARATOR = ";";
    private static final int PACKAGE_DATA_POSITION = 2;

    public static Package parseWialonString(String response) throws Exception {
        String[] parts = response.split("#");
        switch (parts[1]) {
            case "AP": return new PingPackageResponse();
            case "AL": return new LoginPackageResponse(Integer.parseInt(parts[PACKAGE_DATA_POSITION].trim()));
            case "AB": return new BlackBoxPackageResponse(parts[PACKAGE_DATA_POSITION].trim());
            case "ASD": return new ShortDataPackageResponse(parts[PACKAGE_DATA_POSITION].trim());
            case "AD": return new DataPackageResponse(parts[PACKAGE_DATA_POSITION].trim());
            case "AM": return new MessagePackageResponse(parts[PACKAGE_DATA_POSITION].trim());
            case "M": return ServerMessagePackage.parse(parts[PACKAGE_DATA_POSITION].trim());
        }
        throw new Exception("package type not found");
    }

}
