package ua.com.itinnovations.wialonclient.packages.request;

import ua.com.itinnovations.wialonclient.packages.ServerMessagePackage;

public class ServerMessagePackageRequest extends ServerMessagePackage {

    private String mId;
    private PackageType mType;
    private Command mCommand;
    private String mData;

    public ServerMessagePackageRequest(String id,
                                       PackageType type,
                                       Command command,
                                       String data
    ) {
        mId = id;
        mType = type;
        mCommand = command;
        mData = data;
    }

    public String getId() {
        return mId;
    }

    public PackageType getType() {
        return mType;
    }

    public Command getCommand() {
        return mCommand;
    }

    public String getData() {
        return mData;
    }

    @Override
    public String toString() {
        return "ServerMessagePackageRequest{" +
                "mId='" + mId + '\'' +
                ", mType=" + mType +
                ", mCommand=" + mCommand +
                ", mData='" + mData + '\'' +
                '}';
    }

}
