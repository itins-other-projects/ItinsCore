package ua.com.itinnovations.wialonclient.packages.response;

import java.util.Locale;
import java.util.Objects;

import ua.com.itinnovations.wialonclient.packages.Package;

public class MessagePackageResponse extends Package {

    private static final String SUCCESS_CODE = "1";

    private boolean mSendResult = false;

    public MessagePackageResponse() {
    }

    public MessagePackageResponse(String enteredData) {
        mSendResult = Objects.equals(SUCCESS_CODE, enteredData);
    }

    public boolean isSuccessfulSending() {
        return mSendResult;
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "#AM#%d%s", (mSendResult ? 1 : 0), END_OF_LINE);
    }

}
