package ua.com.itinnovations.wialonclient.packages.request;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import ua.com.itinnovations.wialonclient.packages.Package;

public class ShortDataPackageRequest extends Package {

    private Calendar mDateTime;
    private double mLatitude;
    private double mLongitude;
    private int mSpeed;
    private int mCourse;
    private int mAltitude;
    private int mSatellites;

    public ShortDataPackageRequest(Calendar dateTime,
                                   double latitude,
                                   double longitude,
                                   int speed,
                                   int course,
                                   int altitude,
                                   int satellites
    ) {

        mDateTime = dateTime;
        mLatitude = latitude;
        mLongitude = longitude;
        mSpeed = speed;
        mCourse = course;
        mAltitude = altitude;
        mSatellites = satellites;
    }

    private String getDate() {
        SimpleDateFormat format = new SimpleDateFormat("ddMMyy", Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(mDateTime.getTime());
    }

    private String getTime() {
        SimpleDateFormat format = new SimpleDateFormat("HHmmss", Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(mDateTime.getTime());
    }

    String toStringWithoutHeader() {
        return String.format(
                Locale.getDefault(),
                "%s;%s;%s;N;%s;E;%d;%d;%d;%d",
                getDate(),
                getTime(),
                convertLat(mLatitude),
                convertLon(mLongitude),
                mSpeed,
                mCourse,
                mAltitude,
                mSatellites
        ).replace(",", ".");
    }

    private String convertLon(double coordinate) {
        String[] parts = Double.toString(coordinate).split("\\.");
        double rightValue = Double.parseDouble("0." + parts[1]) * 60;
        return (rightValue < 10) ?
                String.format("0%s0%s", parts[0], Double.toString(rightValue)) :
                String.format("0%s%s", parts[0], Double.toString(rightValue));
    }

    private String convertLat(double coordinate) {
        String[] parts = Double.toString(coordinate).split("\\.");
        double rightValue = Double.parseDouble("0." + parts[1]) * 60;
        return (rightValue < 10) ?
                String.format("%s0%s", parts[0], Double.toString(rightValue)) :
                String.format("%s%s", parts[0], Double.toString(rightValue));
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "#SD#%s;%s", toStringWithoutHeader(), END_OF_LINE);
    }

}
