package ua.com.itinnovations.wialonclient.packages.response;

import ua.com.itinnovations.wialonclient.packages.Package;

public class BlackBoxPackageResponse extends Package {

    private int mCountReceivedPackages;

    public BlackBoxPackageResponse(String countReceivedPackages) {
        mCountReceivedPackages = Integer.parseInt(countReceivedPackages);
    }

    public int getCountReceivedPackages() {
        return mCountReceivedPackages;
    }

}
