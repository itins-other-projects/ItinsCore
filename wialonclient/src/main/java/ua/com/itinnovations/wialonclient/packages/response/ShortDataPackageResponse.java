package ua.com.itinnovations.wialonclient.packages.response;

import java.util.Objects;

import ua.com.itinnovations.wialonclient.packages.Package;

public class ShortDataPackageResponse extends Package {

    private String mResponse = "";

    public ShortDataPackageResponse(String response) {
        mResponse = response;
    }

    public boolean isSuccessfullySent() {
        return Objects.equals(mResponse, "1");
    }

    public String getResultCode() {
        return mResponse;
    }

}
