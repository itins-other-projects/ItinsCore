package ua.com.itinnovations.wialonclient.packages.response;

import ua.com.itinnovations.wialonclient.packages.Package;

public class LoginPackageResponse extends Package {

    private static final int SUCCESS_LOGIN = 1;

    private boolean mLogged = false;

    public LoginPackageResponse(int loggedIn) {
        mLogged = SUCCESS_LOGIN == loggedIn;
    }

    public boolean isLoggedIn() {
        return mLogged;
    }

}
