package ua.com.itinnovations.wialonclient.packages.request;

import ua.com.itinnovations.wialonclient.packages.Package;

public class PingPackageRequest extends Package {

    public PingPackageRequest() {
    }

    @Override
    public String toString() {
        return String.format("#P#%s", END_OF_LINE);
    }
}
