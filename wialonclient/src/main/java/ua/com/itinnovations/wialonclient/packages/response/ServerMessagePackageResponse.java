package ua.com.itinnovations.wialonclient.packages.response;

import ua.com.itinnovations.wialonclient.packages.ServerMessagePackage;

public class ServerMessagePackageResponse extends ServerMessagePackage {

    private String mId;
    private Status mStatus;

    public ServerMessagePackageResponse(String id, Status status) {
        mId = id;
        mStatus = status;
    }

    public String getId() {
        return mId;
    }

    public Status getStatus() {
        return mStatus;
    }

}
