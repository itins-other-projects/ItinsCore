package ua.com.itinnovations.wialonclient.client.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayOutputStream;

import ua.com.itinnovations.socketclient.SocketClient;
import ua.com.itinnovations.socketclient.impl.SocketClientImpl;
import ua.com.itinnovations.wialonclient.client.WialonClient;
import ua.com.itinnovations.wialonclient.packages.request.LoginPackageRequest;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SocketClientImpl.class)
public class WialonClientImplTest {

    @Test
    public void testIsStubbed() throws Exception {
        WialonClient wialonClient = new WialonClientImpl();
        Assert.assertTrue(wialonClient.isStubbed());
    }

    @Test
    public void testIsConnectedToServer() throws Exception {
        WialonClient wialonClient1 = new WialonClientImpl();
        Assert.assertFalse(wialonClient1.isConnectedToServer());

        SocketClient socketClient = mock(SocketClientImpl.class);
        when(socketClient.isConnected()).thenReturn(true);
        doNothing().when(socketClient).connect();
        WialonClient wialonClient2 =
                new WialonClientImpl(socketClient, "login", "password");
        Assert.assertTrue(wialonClient2.isConnectedToServer());
    }

    @Test
    public void testSendLoginRequest() throws Exception {
        ByteArrayOutputStream writer = new ByteArrayOutputStream();

        SocketClientImpl socketClient = PowerMockito.mock(SocketClientImpl.class);
        when(socketClient.isConnected()).thenReturn(true);
        doAnswer(invocation -> {
            String value = invocation.getArgument(0);
            writer.write(value.getBytes());
            return null;
        }).when(socketClient).write(isA(String.class));
        doNothing().when(socketClient).connect();

        WialonClient wialonClient =
                new WialonClientImpl(socketClient, "login", "password");
        wialonClient.login("login", "password");

        String expectedLoginString = new LoginPackageRequest("login", "password").toString();
        String actualLoginString = writer.toString();

        Assert.assertEquals("Login packages: ", expectedLoginString, actualLoginString);
    }

//    @Test
//    public void testHandleResponseFromServer() throws Exception {
//
//    }
//
//    @Test
//    public void testKeepSocketAlive() throws Exception {
//
//    }

}
