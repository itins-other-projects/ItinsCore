package ua.com.itinnovations.wialonclient.packages;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ua.com.itinnovations.wialonclient.packages.request.ServerMessagePackageRequest;
import ua.com.itinnovations.wialonclient.packages.response.BlackBoxPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.DataPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.LoginPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.MessagePackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.PingPackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.ServerMessagePackageResponse;
import ua.com.itinnovations.wialonclient.packages.response.ShortDataPackageResponse;

public class PackageTest {

    @Rule
    public ExpectedException mExpectedException = ExpectedException.none();

    @Test
    public void testWialonResponseParserPingPackage() throws Exception {
        String response = "#AP#\r\n";
        Package aPackage = Package.parseWialonString(response);
        Assert.assertTrue(aPackage instanceof PingPackageResponse);
    }

    @Test
    public void testWialonResponseParserLoginPackage() throws Exception {
        String response = "#AL#1\r\n";
        Package aPackage = Package.parseWialonString(response);
        Assert.assertTrue(aPackage instanceof LoginPackageResponse);
        Assert.assertTrue(((LoginPackageResponse) aPackage).isLoggedIn());
    }

    @Test
    public void testWialonResponseParserBlackBoxPackage() throws Exception {
        String response = "#AB#3\r\n";
        Package aPackage = Package.parseWialonString(response);
        Assert.assertTrue(aPackage instanceof BlackBoxPackageResponse);
        Assert.assertEquals(3, ((BlackBoxPackageResponse) aPackage).getCountReceivedPackages());
    }

    @Test
    public void testWialonResponseParserDataPackage() throws Exception {
        String response = "#AD#-1\r\n";
        Package aPackage = Package.parseWialonString(response);
        Assert.assertTrue(aPackage instanceof DataPackageResponse);
        Assert.assertFalse(((DataPackageResponse) aPackage).isSuccessfullySent());
    }

    @Test
    public void testWialonResponseParserShortDataPackage() throws Exception {
        String response = "#ASD#1\r\n";
        Package aPackage = Package.parseWialonString(response);
        Assert.assertTrue(aPackage instanceof ShortDataPackageResponse);
        Assert.assertTrue(((ShortDataPackageResponse) aPackage).isSuccessfullySent());
    }

    @Test
    public void testWialonRequestParserServerMessagePackage() throws Exception {
        String request1 = "#M#123456789;REQ;MSG;{\"id\": \"sfjb34i5205u34tsdgh534535645rfdghrj\"}\r\n";
        Package aPackage1 = Package.parseWialonString(request1);
        Assert.assertTrue(aPackage1 instanceof ServerMessagePackageRequest);

        String request2 = "#M#123456789;RES;OK;{\"id\": \"13456789123456789\"}\r\n";
        Package aPackage2 = Package.parseWialonString(request2);
        Assert.assertTrue(aPackage2 instanceof ServerMessagePackageResponse);
    }

    @Test
    public void testWialonResponseServerMessagePackage() throws Exception {
        String response = "#AM#1\r\n";
        Package aPackage = Package.parseWialonString(response);
        Assert.assertTrue(aPackage instanceof MessagePackageResponse);
        Assert.assertTrue(((MessagePackageResponse) aPackage).isSuccessfulSending());

        String response1 = "#AM#0\r\n";
        Package aPackage1 = Package.parseWialonString(response1);
        Assert.assertTrue(aPackage1 instanceof MessagePackageResponse);
        Assert.assertFalse(((MessagePackageResponse) aPackage1).isSuccessfulSending());
    }

    @Test
    public void testWialonResponseParserNotFoundPackage() throws Exception {
        String response = "#D#1\r\n";
        mExpectedException.expect(Exception.class);
        mExpectedException.expectMessage("package type not found");
        Package.parseWialonString(response);
    }

}