package ua.com.itinnovations.wialonclient.packages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestTrackerPointPojo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("is_sent")
    @Expose
    private Integer isSent;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("datestamp")
    @Expose
    private Integer datestamp;
    @SerializedName("speed")
    @Expose
    private Integer speed;
    @SerializedName("course")
    @Expose
    private Integer course;
    @SerializedName("satellites")
    @Expose
    private Integer satellites;
    @SerializedName("hdop")
    @Expose
    private Integer hdop;
    @SerializedName("inputs")
    @Expose
    private Integer inputs;
    @SerializedName("outputs")
    @Expose
    private Integer outputs;
    @SerializedName("ads")
    @Expose
    private String ads;
    @SerializedName("ibuttons")
    @Expose
    private String ibuttons;
    @SerializedName("deviceStatus")
    @Expose
    private DeviceStatus deviceStatus;


    public TestTrackerPointPojo(Integer id,
                                Integer isSent,
                                Double longitude,
                                Double latitude,
                                Integer datestamp,
                                Integer speed,
                                Integer course,
                                Integer satellites,
                                Integer hdop,
                                Integer inputs,
                                Integer outputs,
                                String ads,
                                String ibuttons,
                                DeviceStatus deviceStatus) {
        super();
        this.id = id;
        this.isSent = isSent;
        this.longitude = longitude;
        this.latitude = latitude;
        this.datestamp = datestamp;
        this.speed = speed;
        this.course = course;
        this.satellites = satellites;
        this.hdop = hdop;
        this.inputs = inputs;
        this.outputs = outputs;
        this.ads = ads;
        this.ibuttons = ibuttons;
        this.deviceStatus = deviceStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsSent() {
        return isSent;
    }

    public void setIsSent(Integer isSent) {
        this.isSent = isSent;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getDatestamp() {
        return datestamp;
    }

    public void setDatestamp(Integer datestamp) {
        this.datestamp = datestamp;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getSatellites() {
        return satellites;
    }

    public void setSatellites(Integer satellites) {
        this.satellites = satellites;
    }

    public Integer getHdop() {
        return hdop;
    }

    public void setHdop(Integer hdop) {
        this.hdop = hdop;
    }

    public Integer getInputs() {
        return inputs;
    }

    public void setInputs(Integer inputs) {
        this.inputs = inputs;
    }

    public Integer getOutputs() {
        return outputs;
    }

    public void setOutputs(Integer outputs) {
        this.outputs = outputs;
    }

    public String getAds() {
        return ads;
    }

    public void setAds(String ads) {
        this.ads = ads;
    }

    public String getIbuttons() {
        return ibuttons;
    }

    public void setIbuttons(String ibuttons) {
        this.ibuttons = ibuttons;
    }

    public DeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(DeviceStatus deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

}


class DeviceStatus {

    @SerializedName("battery_level")
    @Expose
    private Integer batteryLevel;
    @SerializedName("is_charging")
    @Expose
    private Integer isCharging;
    @SerializedName("gsm_level")
    @Expose
    private Integer gsmLevel;

    public DeviceStatus(Integer batteryLevel, Integer isCharging, Integer gsmLevel) {
        super();
        this.batteryLevel = batteryLevel;
        this.isCharging = isCharging;
        this.gsmLevel = gsmLevel;
    }

    public Integer getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(Integer batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public Integer getIsCharging() {
        return isCharging;
    }

    public void setIsCharging(Integer isCharging) {
        this.isCharging = isCharging;
    }

    public Integer getGsmLevel() {
        return gsmLevel;
    }

    public void setGsmLevel(Integer gsmLevel) {
        this.gsmLevel = gsmLevel;
    }

}

