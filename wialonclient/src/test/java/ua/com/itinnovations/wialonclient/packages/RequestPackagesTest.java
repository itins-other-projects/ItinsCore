package ua.com.itinnovations.wialonclient.packages;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ua.com.itinnovations.wialonclient.packages.request.BlackBoxPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.DataPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.DataPackageRequest.Param;
import ua.com.itinnovations.wialonclient.packages.request.PingPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.ServerMessagePackageRequest;

@Ignore
public class RequestPackagesTest {

    private List<TestTrackerPointPojo> mTestTrackerPointPojos;

    @Before
    public void setUp() throws Exception {
        Type type = new TypeToken<List<TestTrackerPointPojo>>() {}.getType();
        mTestTrackerPointPojos = new GsonBuilder().create().fromJson("", type);
    }

    @Test
    public void testBlackBoxPackageRequest() throws Exception {

        List<DataPackageRequest> dataPackages = new ArrayList<>();
        /// testTrackerPointPojos.size() == 25 --- received from blackboxprepareddata.json. Cuz big size, I limited
        /// count of packages with 10
        for (int i = 0; i < 10; i++) {
            TestTrackerPointPojo testTrackerPointPojo = mTestTrackerPointPojos.get(i);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(testTrackerPointPojo.getDatestamp());
            ArrayList<Param> params = new ArrayList<>();
            params.add(new Param("id67", 1, String.valueOf(testTrackerPointPojo.getDeviceStatus().getBatteryLevel())));
            params.add(new Param("id66", 1, String.valueOf(testTrackerPointPojo.getDeviceStatus().getIsCharging())));
            DataPackageRequest dataPackage = new DataPackageRequest(
                    calendar,
                    testTrackerPointPojo.getLatitude(),
                    testTrackerPointPojo.getLongitude(),
                    testTrackerPointPojo.getSpeed(),
                    testTrackerPointPojo.getCourse(),
                    0,
                    10,
                    0.0F,
                    0,
                    0,
                    "",
                    "",
                    params
            );
            dataPackages.add(dataPackage);
        }
        BlackBoxPackageRequest<DataPackageRequest> blackBoxPackage = new BlackBoxPackageRequest<>(dataPackages);
        String actualValue = blackBoxPackage.toString();
    }

    @Test
    public void testClientMessagePackageRequest() throws Exception {
    }

    @Test
    public void testDataPackageRequest() throws Exception {
        String expectedValue = "#D#%s;%s";




        TestTrackerPointPojo testTrackerPointPojo = mTestTrackerPointPojos.get(0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(testTrackerPointPojo.getDatestamp());
        ArrayList<Param> params = new ArrayList<>();
        params.add(new Param("id67", 1, String.valueOf(testTrackerPointPojo.getDeviceStatus().getBatteryLevel())));
        params.add(new Param("id66", 1, String.valueOf(testTrackerPointPojo.getDeviceStatus().getIsCharging())));
        DataPackageRequest dataPackage = new DataPackageRequest(
                calendar,
                testTrackerPointPojo.getLatitude(),
                testTrackerPointPojo.getLongitude(),
                testTrackerPointPojo.getSpeed(),
                testTrackerPointPojo.getCourse(),
                0,
                10,
                0.0F,
                0,
                0,
                "",
                "",
                params
        );

//        Assert.assertEquals(dataPackage.toString());

    }

    @Test
    public void testPingPackageRequest() throws Exception {
        PingPackageRequest pingPackage = new PingPackageRequest();
        Assert.assertEquals(pingPackage.toString(), "#P#\r\n");
    }

    @Test
    public void testServerMessagePackageRequest() throws Exception {
        String id = "1234567";
        MessagePackage.PackageType packageType = MessagePackage.PackageType.REQUEST;
        MessagePackage.Command command = MessagePackage.Command.INTERVAL;
        String data = "Data";
        ServerMessagePackageRequest serverMessagePackageRequest =
                new ServerMessagePackageRequest(id, packageType, command, data);
        Assert.assertEquals(serverMessagePackageRequest.getId(), id);
        Assert.assertEquals(serverMessagePackageRequest.getData(), data);
        Assert.assertEquals(serverMessagePackageRequest.getType(), packageType);
    }

}
