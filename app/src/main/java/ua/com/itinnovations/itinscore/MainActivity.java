package ua.com.itinnovations.itinscore;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import ua.com.itinnovations.logger.Logger;
import ua.com.itinnovations.logger.impl.ConsoleLoggerImpl;
import ua.com.itinnovations.socketclient.SocketClient;
import ua.com.itinnovations.socketclient.impl.SocketClientImpl;
import ua.com.itinnovations.tracker.Config;
import ua.com.itinnovations.tracker.Tracker;
import ua.com.itinnovations.tracker.TrackerImpl;
import ua.com.itinnovations.tracker.TrackerWialonSender;
import ua.com.itinnovations.tracker.database.TrackerDatabase;
import ua.com.itinnovations.tracker.filters.TrackerPointFilter;
import ua.com.itinnovations.tracker.filters.TrackerStorage;
import ua.com.itinnovations.tracker.filters.base.AbstractTrackerPointFilter;
import ua.com.itinnovations.tracker.filters.impl.BearingTrackerPointFilterImpl;
import ua.com.itinnovations.tracker.filters.impl.DistanceTrackerPointFilterImpl;
import ua.com.itinnovations.tracker.filters.impl.SmartParkingTrackerPointFilterImpl;
import ua.com.itinnovations.tracker.filters.impl.TimerTrackerPointFilterImpl;
import ua.com.itinnovations.wialonclient.client.WialonClient;
import ua.com.itinnovations.wialonclient.client.impl.WialonClientImpl;
import ua.com.itinnovations.wialonclient.packages.response.LoginPackageResponse;

import static ua.com.itinnovations.wialonclient.client.WialonClient.CONNECTION_STATE_CONSUMER_NAME;

public class MainActivity extends AppCompatActivity {

    private GoogleApiClient mGoogleApiClient;
    private LocationProvider mLocationProvider;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(getGoogleApiConnectionCallbacks())
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mLocationProvider) {
            mLocationProvider.destroy();
        }
        if (null != mGoogleApiClient) {
            mGoogleApiClient.disconnect();
        }
    }

    @NonNull
    private GoogleApiClientConnectionCallbacks getGoogleApiConnectionCallbacks() {
        return (bundle) -> {
            SocketClient client = new SocketClientImpl("con1.vzo.com.ua", 5555);
            WialonClient wialonClient = new WialonClientImpl(client, "863812021380765", "IPS");

            wialonClient.addReceiver(CONNECTION_STATE_CONSUMER_NAME, aPackage -> {
                String loginState = String.valueOf(((LoginPackageResponse) aPackage).isLoggedIn());
                Log.wtf(CONNECTION_STATE_CONSUMER_NAME, loginState);
            });

            mTracker = getTracker(wialonClient);
            mTracker.startSender();
            mLocationProvider = new LocationProvider(mGoogleApiClient);
            mLocationProvider.provide().subscribe(mTracker::handleLocation);
        };
    }

    private Tracker getTracker(WialonClient wialonClient) {
        Config trackerConfig = new Config();

        RoomDatabase.Builder<TrackerDatabase> databaseBuilder = Room.databaseBuilder(
                getApplicationContext(),
                TrackerDatabase.class,
                TrackerDatabase.NAME
        );
        TrackerDatabase trackerDatabase = databaseBuilder.build();

        Logger logger = new ConsoleLoggerImpl(Logger.LogLevel.WTF);

        TrackerWialonSender sender = new TrackerWialonSender(
                trackerConfig, wialonClient, trackerDatabase.getTrackerPointDao(), logger);

        Tracker tracker = new TrackerImpl(trackerConfig, sender, logger);

        TrackerStorage storage = new TrackerStorage();

        AbstractTrackerPointFilter timerFilter = new TimerTrackerPointFilterImpl(
                trackerConfig.getMoveTimer(), storage, null, trackerDatabase.getTrackerPointDao());
        AbstractTrackerPointFilter distanceFilter = new DistanceTrackerPointFilterImpl(
                trackerConfig.getDistance(), storage, null, trackerDatabase.getTrackerPointDao());
        AbstractTrackerPointFilter bearingFilter = new BearingTrackerPointFilterImpl(
                trackerConfig.getBearing(), storage, null, trackerDatabase.getTrackerPointDao());
        AbstractTrackerPointFilter smartParkingFilter = new SmartParkingTrackerPointFilterImpl(
                trackerConfig.getMoveTimer(), storage, null, trackerDatabase.getTrackerPointDao());

        /** DO NOT ADD `SmartParkingTrackerPointFilter` IN `filtes` LIST **/
        List<TrackerPointFilter> filters = new ArrayList<>();
        filters.add(timerFilter);
        filters.add(bearingFilter);
        filters.add(distanceFilter);

        ((TrackerImpl) tracker).setTrackerPointFilters(filters);
        ((TrackerImpl) tracker).setTimerTrackerPointFilter(timerFilter);
        ((TrackerImpl) tracker).setSmartParkingTrackerPointFilter(smartParkingFilter);
        return tracker;
    }


    private interface GoogleApiClientConnectionCallbacks extends GoogleApiClient.ConnectionCallbacks {
        @Override
        default void onConnectionSuspended(int i) {
        }
    }

}
