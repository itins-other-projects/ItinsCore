package ua.com.itinnovations.itinscore;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.amitshekhar.DebugDB;

public class App extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this);

        DebugDB.initialize(this);
        Log.wtf("DATABASE", DebugDB.getAddressLog());
    }
}
