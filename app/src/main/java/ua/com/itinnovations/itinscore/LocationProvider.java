package ua.com.itinnovations.itinscore;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

class LocationProvider {

    private static final long LOCATION_UPDATE_INTERVAL = 1000L;

    private GoogleApiClient mGoogleApiClient;
    private PublishSubject<Location> mSubject = PublishSubject.create();
    private LocationListener mLocationListener = this::provideNewLocation;

    LocationProvider(GoogleApiClient googleApiClient) {
        mGoogleApiClient = googleApiClient;
    }

    LocationProvider provide() {
        requestFusedLocationUpdates();
        return this;
    }

    void subscribe(Consumer<Location> consumer) {
        mSubject.subscribe(consumer);
    }

    void destroy() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                mLocationListener
        );
    }

    private void requestFusedLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                getRequest(),
                mLocationListener
        );
    }

    private void provideNewLocation(Location location) {
        mSubject.onNext(location);
    }

    private LocationRequest getRequest() {
        return new LocationRequest()
                .setInterval(LOCATION_UPDATE_INTERVAL)
                .setFastestInterval(LOCATION_UPDATE_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

}
