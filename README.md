# ItinsCore   [![](https://jitpack.io/v/com.gitlab.itins-other-projects/ItinsCore.svg)](https://jitpack.io/#com.gitlab.itins-other-projects/ItinsCore)


To include this core into project, firstly place this in project's build.gradle
file in section allprojects.repositories. 

```groovy
	allprojects {
		repositories {
			maven {
				url 'https://jitpack.io'
				credentials { username "jp_ade8ner265tthvl80jcsannlo4" }
			}
		}
	}
```

You need be invited to this repo, for using it. So if it's not truth, firstly ask
an invite.

List of core modules: 

`deviceinfo`      -- this module provides an info about state of device, such as
battery level or gsm strength. It moslty used in tracker, for separating this logic
from it. But, if you need this info in your code, you can use this module.
To add this module to project, place this dependency to your module gradle build file:
```groovy
    implementation 'com.gitlab.itins-other-projects.ItinsCore:deviceinfo:1.3.1'
```


`bluetooth`       -- bluetooth client for android. For now this client can only read
from stream and provides its data tou subscribers. Also, you don't need to manage
bluetooth socket yourself. This module will connect and reconnect it by itself.
To add this module to project, place this dependency to your module gradle build file:
```groovy
    implementation 'com.gitlab.itins-other-projects.ItinsCore:bluetooth:1.3.1'
```


`socketclient`    -- socket client for connections to server. Manage connection
inside it.
To add this module to project, place this dependency to your module gradle build file:
```groovy
    implementation 'com.gitlab.itins-other-projects.ItinsCore:socketclient:1.3.1'
```


`wialonclient`    -- an android client for IPS (Wialon) server. Has some additional
features as message-packages for sending custom messages.
To add this module to project, place this dependency to your module gradle build file:
```groovy
    implementation 'com.gitlab.itins-other-projects.ItinsCore:wialonclient:1.3.1'
```


`tracker`         -- android based tracker, for collecting locations from GPS and
filter them by special filters (distance, timer ...)
To add this module to project, place this dependency to your module gradle build file:
```groovy
    implementation 'com.gitlab.itins-other-projects.ItinsCore:tracker:1.3.1'
```


`messages`        -- to be implemented


`ftpclient`       -- ftp client with possibility to uploading and downloading files
to/from server. This module based on apache commons-net library.
To add this module to project, place this dependency to your module gradle build file:
```groovy
    implementation 'com.gitlab.itins-other-projects.ItinsCore:ftpclient:1.3.1'
```


`license`         -- this module is necessary, when you need to implement licensing in
your app. So, you can create license file with json (which represents pojo in your code),
encoded with sha algorithm (encoding key look inside module in class LicenseManager.
To add this module to project, place this dependency to your module gradle build file:
```groovy
    implementation 'com.gitlab.itins-other-projects.ItinsCore:license:1.3.1'
```


`locationprovider`-- this module provides location providers, which, using google play
services or bluetooth, reads data about current user's location and provides this info
to your high-level code. Possible to use with external bluetooth antenna and google
location services.
To add this module to project, place this dependency to your module gradle build file:
```groovy
    implementation 'com.gitlab.itins-other-projects.ItinsCore:locationprovider:1.3.1'
```


`logger`          -- logger, for log situation of Itins apps in production. Has
several varians of logging (to file, to logcat) with several levels (info, debug...)
To add this module to project, place this dependency to your module gradle build file:
```groovy
    implementation 'com.gitlab.itins-other-projects.ItinsCore:logger:1.3.1'
```


`externalsensors` -- to be implemented

`appfilesadmin`   -- to be implemented



Useful links: 
- [List of all versions, built in JitPack](https://jitpack.io/api/builds/com.gitlab.itins-other-projects/ItinsCore)
- [This library on JitPack](https://jitpack.io/#com.gitlab.itins-other-projects/ItinsCore)
