package ua.com.itinnovations.bluetooth.bluetoothclient.impl;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ua.com.itinnovations.bluetooth.bluetoothclient.BluetoothClient;
import ua.com.itinnovations.bluetooth.bluetoothclient.BluetoothReceiver;

public class BluetoothReceiverImpl extends BroadcastReceiver implements BluetoothReceiver {

    private BluetoothClient mBluetoothClient;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDiscoveryListener mBluetoothDiscoveryListener;

    public BluetoothReceiverImpl(BluetoothClient bluetoothClient) {
        mBluetoothClient = bluetoothClient;
        mBluetoothClient.setSocketClientStateListener(getSocketClientStateListener());
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String stringAction = getDeviceAction(intent);
        handleDeviceAction(stringAction, intent);
    }

    @Override
    public void setBluetoothAdapter(BluetoothAdapter bluetoothAdapter) {
        mBluetoothAdapter = bluetoothAdapter;
    }

    @Override
    public void setBluetoothDiscoveryListener(BluetoothDiscoveryListener discoveryListener) {
        mBluetoothDiscoveryListener = discoveryListener;
    }

    @Override
    public void startDiscovering() {
        if (!mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.startDiscovery();
        }
    }

    @Override
    public void stopDiscovering() {
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
    }

    private BluetoothClient.SocketClientStateListener getSocketClientStateListener() {
        return new BluetoothClient.SocketClientStateListener() {
            @Override
            public void onSocketConnected() {
                stopDiscovering();
            }

            @Override
            public void onSocketConnecting() {
                stopDiscovering();
            }

            @Override
            public void onSocketDisconnected() {
                startDiscovering();
            }
        };
    }

    /**
     * Handle parsed action from receiver. Check is action is found device, changed internal
     * bluetooth state, device disconnected, start or finish discovering and throws this
     * actions to special methods, where it will be handled
     */
    private void handleDeviceAction(String stringAction, Intent intent) {
        if (BluetoothDevice.ACTION_FOUND.equals(stringAction)) {
            handleActionFoundDevices(intent);
        } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(stringAction)) {
            handleActionExternalDeviceDisconnected();
        } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(stringAction)) {
            handleActionDeviceBluetoothStateChanged(intent);
        } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(stringAction)) {
            handleActionDiscoveringStarted();
        } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(stringAction)) {
            handleActionDiscoveringFinished();
        } else {
            handleErrorsAcquiredDuringDiscovering();
        }
    }

    private String getDeviceAction(Intent intent) {
        return (null != intent.getAction()) ? intent.getAction() : "";
    }

    /**
     * Found new device. Add it to client, where will be an interface
     * for manipulating data in bl-socket
     */
    private void handleActionFoundDevices(Intent intent) {
        BluetoothDevice device = getBluetoothDevice(intent);
        if (null != device) {
            mBluetoothClient.setDevice(device);
            stopDiscovering();
        }
    }

    /**
     * The connected device is disconnected now, need to reconnect it
     */
    private void handleActionExternalDeviceDisconnected() {
        startDiscovering();
    }

    /**
     * Discovering finished. If devices not found before this time, then
     * is need to notify about this
     *
     */
    private void handleActionDiscoveringFinished() {
        if (mBluetoothClient.isNoneState() && (null != mBluetoothDiscoveryListener)) {
            mBluetoothDiscoveryListener.oExternalDevicesNotFound();
        }
    }

    private void handleActionDiscoveringStarted() {
        if (mBluetoothClient.isConnected()) {
            stopDiscovering();
        }
    }

    /**
     * State of internal bl (on device) changed. Need to detect, is it
     * disabled, then restart it, or if it enabled, start discovering
     * external bl-devices
     */
    private void handleActionDeviceBluetoothStateChanged(Intent intent) {
        int state = getDeviceBluetoothState(intent);
        if (BluetoothAdapter.STATE_ON == state) {
            handleBluetoothStateEnabled();
        } else if (BluetoothAdapter.STATE_TURNING_OFF == state) {
            handleBluetoothDisabling();
        } else if (BluetoothAdapter.ERROR == state) {
            handleBluetoothManipulatingErrors();
        }
    }

    /**
     * Start discovering external devices, if internal bl is enabled
     */
    private void handleBluetoothStateEnabled() {
        startDiscovering();
    }

    /**
     * If internal bl is disabled, then run it. For this notify activity,
     * that is necessary to enable bl
     */
    private void handleBluetoothDisabling() {
        if (null != mBluetoothDiscoveryListener) {
            mBluetoothDiscoveryListener.onBluetoothDisconnected();
        }
    }

    /**
     * An error acquired during manipulating internal bl
     */
    private void handleBluetoothManipulatingErrors() {
    }

    /**
     * An error handlers here. If some errors acquire during discovering, then
     * here them should be handled
     */
    private void handleErrorsAcquiredDuringDiscovering() {
        // for this time is nothing here, cuz it is not info about, what can
        // happened, so it will be necessary to handle acquired situations here
    }

    private BluetoothDevice getBluetoothDevice(Intent intent) {
        return intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
    }

    private int getDeviceBluetoothState(Intent intent) {
        return intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
    }

}
