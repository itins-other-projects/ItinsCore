package ua.com.itinnovations.bluetooth.bluetoothclient;

import android.bluetooth.BluetoothAdapter;

public interface BluetoothReceiver {

    void startDiscovering();

    void stopDiscovering();

    void setBluetoothAdapter(BluetoothAdapter bluetoothAdapter);

    void setBluetoothDiscoveryListener(BluetoothDiscoveryListener discoveryListener);

    interface BluetoothDiscoveryListener {

        void onBluetoothDisconnected();

        void oExternalDevicesNotFound();

    }

}
