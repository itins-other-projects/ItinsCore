package ua.com.itinnovations.bluetooth.bluetoothclient;

import android.bluetooth.BluetoothDevice;

import java.util.UUID;

public interface BluetoothClient {

    enum ConnectionState {
        CONNECTING, CONNECTED, NONE;
    }

    enum ConnectingState {
        CONNECTED, CANCELED, CONNECTING
    }

    ConnectionState getState();

    boolean isConnected();

    boolean isNoneState();

    void setDevice(BluetoothDevice device);

    void setDeviceUuid(UUID uuid);

    void setDeviceDataListener(DeviceDataListener deviceDataListener);

    void setSocketClientStateListener(SocketClientStateListener listener);


    @FunctionalInterface
    interface DeviceDataListener {

        void observe(String data);

    }


    interface SocketClientStateListener {

        void onSocketConnected();

        void onSocketConnecting();

        void onSocketDisconnected();

    }

}
