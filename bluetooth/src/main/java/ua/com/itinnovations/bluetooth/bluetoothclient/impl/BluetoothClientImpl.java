package ua.com.itinnovations.bluetooth.bluetoothclient.impl;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

import ua.com.itinnovations.bluetooth.bluetoothclient.BluetoothClient;

public class BluetoothClientImpl implements BluetoothClient {

    private static final String DEFAULT_MAC = "00:00:00:00:00:00";
    private static final long DEFAULT_READING_TIMEOUT = 1000L;

    private long mReadingTimeout;

    private String mExternalDeviceMac;
    private ConnectionState mState;
    private BluetoothDevice mSelectedDevice;
    private DeviceDataListener mDeviceDataListener;
    private SocketClientStateListener mSocketClientStateListener;
    private UUID mDeviceUuid;

    private ConnectingThread mConnectingThread;
    private DataInteractorThread mDataInteractorThread;

    public BluetoothClientImpl() {
        this(ConnectionState.NONE, UUID.randomUUID(), DEFAULT_READING_TIMEOUT, DEFAULT_MAC);
    }

    public BluetoothClientImpl(UUID deviceUuid,
                               long readingTimeout,
                               String externalDeviceMac) {
        this(ConnectionState.NONE, deviceUuid, readingTimeout, externalDeviceMac);
    }

    public BluetoothClientImpl(ConnectionState state,
                               UUID deviceUuid,
                               long readingTimeout,
                               String externalDeviceMac) {
        mReadingTimeout = readingTimeout;
        mState = state;
        mDeviceUuid = deviceUuid;
        mExternalDeviceMac = externalDeviceMac;
    }

    @Override
    public void setDevice(BluetoothDevice device) {
        mSelectedDevice = device;
        connectToDevice();
    }

    @Override
    public void setDeviceUuid(UUID uuid) {
        mDeviceUuid = uuid;
    }

    @Override
    public void setDeviceDataListener(DeviceDataListener deviceDataListener) {
        mDeviceDataListener = deviceDataListener;
    }

    @Override
    public void setSocketClientStateListener(SocketClientStateListener listener) {
        mSocketClientStateListener = listener;
    }

    @Override
    public ConnectionState getState() {
        return mState;
    }

    @Override
    public boolean isConnected() {
        return ConnectionState.CONNECTED == mState;
    }

    @Override
    public boolean isNoneState() {
        return ConnectionState.NONE == mState;
    }

    private synchronized void connectToDevice() {
        if (!mSelectedDevice.getAddress().equals(mExternalDeviceMac)) return;

        if ((ConnectionState.CONNECTING == mState) && (null != mConnectingThread)) {
            mConnectingThread.cancel();
            mConnectingThread = null;
        }

        if (null != mDataInteractorThread) {
            mDataInteractorThread.cancel();
            mDataInteractorThread = null;
        }

        try {
            mConnectingThread = new ConnectingThread(mSelectedDevice);
            mState = ConnectionState.CONNECTING;
            mConnectingThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void onConnected(BluetoothSocket socket) {
        if (null != mConnectingThread) {
            mConnectingThread.cancel();
            mConnectingThread = null;
        }

        if (null != mDataInteractorThread) {
            mDataInteractorThread.cancel();
            mDataInteractorThread = null;
        }

        try {
            mDataInteractorThread = new DataInteractorThread(socket);
            mState = ConnectionState.CONNECTED;
            mDataInteractorThread.start();
            mSocketClientStateListener.onSocketConnected();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onConnectionFailed() {
        mState = ConnectionState.NONE;
        mSocketClientStateListener.onSocketDisconnected();
    }

    private void onConnectionLost() {
        mState = ConnectionState.NONE;
        mSocketClientStateListener.onSocketDisconnected();
    }


    private class ConnectingThread extends Thread {

        private static final int RECONNECT_ATTEMPTS = 30;

        private BluetoothSocket mBluetoothSocket;
        private ConnectingState mConnectionState;

        private ConnectingThread(BluetoothDevice device) throws IOException {
            try {
                mBluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(mDeviceUuid);
                mConnectionState = ConnectingState.CONNECTING;
            } catch (IOException ignored) {  }
        }

        @Override
        public void run() {
            mSocketClientStateListener.onSocketConnected();
            int attemptsCounter = 0;
            while (isConnectAttemptPossible(attemptsCounter)) {
                attemptsCounter++;
                tryToConnectToBluetoothSocket();
            }

            synchronized (BluetoothClientImpl.this) {
                if (ConnectingState.CONNECTED == mConnectionState) {
                    onConnected(mBluetoothSocket);
                } else {
                    onConnectionFailed();
                }
            }
        }

        private void tryToConnectToBluetoothSocket() {
            try {
                mBluetoothSocket.connect();
                mConnectionState = ConnectingState.CONNECTED;
            } catch (IOException ignored) {  }
        }

        private boolean isConnectAttemptPossible(int attemptsCounter) {
            return (ConnectingState.CONNECTING == mConnectionState) && (attemptsCounter < RECONNECT_ATTEMPTS);
        }

        private void cancel() {
            mConnectionState = ConnectingState.CANCELED;
        }

    }


    private class DataInteractorThread extends Thread {

        private BluetoothSocket mBluetoothSocketClient;

        private DataInteractorThread(BluetoothSocket bluetoothSocketClient) throws IOException {
            mBluetoothSocketClient = bluetoothSocketClient;
        }

        @Override
        public void run() {
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            byte[] buffer = new byte[4800];
            int length;

            while (isReadingPossible()) {
                try {
                    if (-1 != (length = mBluetoothSocketClient.getInputStream().read(buffer))) {
                        result.write(buffer, 0, length);
                        String string = result.toString("UTF-8");
                        notifyListener(string);
                        result.reset();
                    }
                } catch (IOException e) {
                    onConnectionLost();
                }
                sleep();
            }
        }

        private void notifyListener(String string) {
            if (null != mDeviceDataListener) mDeviceDataListener.observe(string);
        }

        private boolean isReadingPossible() {
            return ConnectionState.CONNECTED == mState;
        }

        private void sleep() {
            try {
                sleep(mReadingTimeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void cancel() {
            try {
                mBluetoothSocketClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
