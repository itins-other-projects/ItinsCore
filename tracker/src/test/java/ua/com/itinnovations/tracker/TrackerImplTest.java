package ua.com.itinnovations.tracker;

//@RunWith(RobolectricTestRunner.class)
//public class TrackerImplTest {
//
//    @Test
//    public void testStartingTrackerSender() throws Exception {
//        Context context = RuntimeEnvironment.application.getApplicationContext();
//        Config config = new Config();
//        TrackerWialonSender sender = Mockito.mock(TrackerWialonSender.class);
//        Tracker tracker = new TrackerImpl(context, config, sender);
//
//        tracker.startSender();
//
//        Assert.assertTrue(sender.isAlive());
//    }

//    @Test
//    public void testHandleLocation() throws Exception {
//        Context context = RuntimeEnvironment.application.getApplicationContext();
//        Config config = new Config();
//        TrackerWialonSender sender = Mockito.mock(TrackerWialonSender.class);
//
//        AbstractTrackerPointFilter timerFilter = Mockito.mock(TimerTrackerPointFilter.class);
//        AbstractTrackerPointFilter bearingFilter = Mockito.mock(BearingTrackerPointFilter.class);
//        AbstractTrackerPointFilter distanceFilter = Mockito.mock(DistanceTrackerPointFilter.class);
//        AbstractTrackerPointFilter smartParkingFilter = Mockito.mock(SmartParkingTrackerPointFilter.class);
//        List<TrackerPointFilter> filters = new ArrayList<>();
//        filters.add(timerFilter);
//        filters.add(bearingFilter);
//        filters.add(distanceFilter);
//        filters.add(smartParkingFilter);
//
//        Location testLocation = new Location("TEST_LOCATION");
//        Mockito.when(timerFilter.handleLocation(testLocation)).thenReturn(false);
//        Mockito.when(bearingFilter.handleLocation(testLocation)).thenReturn(false);
//        Mockito.when(distanceFilter.handleLocation(testLocation)).thenReturn(false);
//        Mockito.when(smartParkingFilter.handleLocation(testLocation)).thenReturn(false);
//
//        Tracker tracker = new TrackerImpl(context, config, sender);
//
//        tracker.setSmartParkingTrackerPointFilter((SmartParkingTrackerPointFilter) smartParkingFilter);
//        tracker.setTimerTrackerPointFilter((TimerTrackerPointFilter) timerFilter);
//        tracker.setTrackerPointFilters(filters);
//
//        Location locationOnStay = new Location("TEST_LOCATION");
//        locationOnStay.setSpeed(0.33F);
//        locationOnStay.setAccuracy(15);
//        locationOnStay.setBearing(32);
//        locationOnStay.setLongitude(30.424078166666668);
//        locationOnStay.setLatitude(50.41479983333333);
//
//        tracker.handleLocation(locationOnStay);
//
//        Location locationOnMove = new Location("TEST_LOCATION");
//        locationOnMove.setSpeed(10);
//        locationOnMove.setAccuracy(15);
//        locationOnMove.setBearing(32);
//        locationOnMove.setLongitude(30.424078);
//        locationOnMove.setLatitude(50.4147998);
//
//        tracker.handleLocation(locationOnMove);
//
//        timerFilter = Mockito.mock(TimerTrackerPointFilter.class);
//        bearingFilter = Mockito.mock(BearingTrackerPointFilter.class);
//        distanceFilter = Mockito.mock(DistanceTrackerPointFilter.class);
//        smartParkingFilter = Mockito.mock(SmartParkingTrackerPointFilter.class);
//        filters = new ArrayList<>();
//        filters.add(timerFilter);
//        filters.add(bearingFilter);
//        filters.add(distanceFilter);
//        filters.add(smartParkingFilter);
//
//        testLocation = new Location("TEST_LOCATION");
//        Mockito.when(timerFilter.handleLocation(testLocation)).thenReturn(false);
//        Mockito.when(bearingFilter.handleLocation(testLocation)).thenReturn(false);
//        Mockito.when(distanceFilter.handleLocation(testLocation)).thenReturn(true);
//        Mockito.when(smartParkingFilter.handleLocation(testLocation)).thenReturn(false);
//        tracker.setSmartParkingTrackerPointFilter((SmartParkingTrackerPointFilter) smartParkingFilter);
//        tracker.setTimerTrackerPointFilter((TimerTrackerPointFilter) timerFilter);
//        tracker.setTrackerPointFilters(filters);
//
//        locationOnMove = new Location("TEST_LOCATION");
//        locationOnMove.setSpeed(9);
//        locationOnMove.setAccuracy(20);
//        locationOnMove.setBearing(11);
//        locationOnMove.setLongitude(31.491150);
//        locationOnMove.setLatitude(50.0013258);
//
//        tracker.handleLocation(locationOnMove);
//    }
//
//}