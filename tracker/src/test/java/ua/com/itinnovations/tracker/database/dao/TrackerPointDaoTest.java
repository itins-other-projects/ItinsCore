package ua.com.itinnovations.tracker.database.dao;

import android.arch.persistence.room.Room;
import android.content.Context;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import ua.com.itinnovations.tracker.BuildConfig;
import ua.com.itinnovations.tracker.database.TrackerDatabase;
import ua.com.itinnovations.tracker.database.entities.TrackerPoint;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class TrackerPointDaoTest {

    private TrackerDatabase mTrackerDatabase;
    private List<TrackerPoint> mTrackerPoints;

    @Before
    public void setUp() throws Exception {
        Context context = Mockito.mock(Context.class);

        mTrackerDatabase = Room.inMemoryDatabaseBuilder(context, TrackerDatabase.class)
                .allowMainThreadQueries()
                .build();

        mTrackerPoints = Arrays.asList(
                new TrackerPoint(24.823995, 50.933260, Calendar.getInstance(), (int) (2.558 * 3.6), 10, 7),
                new TrackerPoint(24.824012, 50.933686, Calendar.getInstance(), (int) (2.762 * 3.6), 14, 7),
                new TrackerPoint(24.824021, 50.933887, Calendar.getInstance(), (int) (3.033 * 3.6), 9, 7),
                new TrackerPoint(24.824180, 50.933945, Calendar.getInstance(), (int) (2.909 * 3.6), 8, 7),
                new TrackerPoint(24.824150, 50.934013, Calendar.getInstance(), (int) (2.763 * 3.6), 5, 7)
        );
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void addNewTrackerPoint() throws Exception {
        TrackerPointDao trackerPointDao = mTrackerDatabase.getTrackerPointDao();
        for (TrackerPoint point: mTrackerPoints) trackerPointDao.addNewTrackerPoint(point);
        List<TrackerPoint> selectedPoints = trackerPointDao.getNotSentLocationsFromDatabase();

        for (int i = 0; i < mTrackerPoints.size(); i++) {
            Calendar dateStampSaved = selectedPoints.get(i).getCurrentDateStamp();
            Calendar dateStampPrepared = mTrackerPoints.get(i).getCurrentDateStamp();

            Assert.assertEquals(dateStampSaved.getTimeInMillis(), dateStampPrepared.getTimeInMillis());
            Assert.assertNotEquals(selectedPoints.get(i).getId(), 0);
        }
    }

    @Test
    public void updateSentStatus() throws Exception {
        int start = 2;
        int last = start + 2;
        TrackerPointDao trackerPointDao = mTrackerDatabase.getTrackerPointDao();
        for (TrackerPoint point: mTrackerPoints) trackerPointDao.addNewTrackerPoint(point);
        List<Long> preparedIds = Arrays.asList(2L, 3L, 4L);

        trackerPointDao.updateSentStatus(start, last);

        List<TrackerPoint> selectedPoints = trackerPointDao.getNotSentLocationsFromDatabase();
        List<Long> ids = selectedPoints.stream().map(TrackerPoint::getId).collect(Collectors.toList());
        preparedIds.forEach(preparedId -> Assert.assertFalse(ids.contains(preparedId)));
    }

    @Test
    public void getNotSentLocationsFromDatabase() throws Exception {
        int start = 1;
        int last = start + mTrackerPoints.size();
        TrackerPointDao trackerPointDao = mTrackerDatabase.getTrackerPointDao();
        for (TrackerPoint point: mTrackerPoints) trackerPointDao.addNewTrackerPoint(point);

        trackerPointDao.updateSentStatus(start, last);

        List<TrackerPoint> selectedPoints = trackerPointDao.getNotSentLocationsFromDatabase();
        Assert.assertTrue(selectedPoints.isEmpty());
    }

}