package ua.com.itinnovations.tracker;

import android.location.Location;

public interface Tracker {

    /**
     * Coefficient for converting speed from km/h to m/s
     */
    double KM_PER_HOUR_TO_M_PER_SEC_COEFFICIENT = 3.6;

    /**
     * Starts tracker wialon sender
     */
    void startSender();

    /**
     * Runs all tracker algorithms.
     *
     * @param location new location, which need to check and store, if necessary
     */
    void handleLocation(Location location);

}
