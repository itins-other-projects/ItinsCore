package ua.com.itinnovations.tracker.utils;

import android.location.Location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ua.com.itinnovations.deviceinfo.DeviceInfo;
import ua.com.itinnovations.tracker.database.entities.TrackerPoint;
import ua.com.itinnovations.wialonclient.packages.request.BlackBoxPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.DataPackageRequest;
import ua.com.itinnovations.wialonclient.packages.request.DataPackageRequest.Param;

/**
 * Some tracker-utils for getting angle or satellites for example
 *
 * @author sergey
 * @since 02/03/18 11:10 AM
 */
public class TrackerUtils {

    /**
     * Key for sending value of battery to server.
     * Data sends as long int value
     */
    private static final String PARAM_KEY_BATTERY_LEVEL = "par67";
    /**
     * Key for sending value of GSM to server. Data to server about GSM
     * sends as int value
     */
    private static final String PARAM_KEY_GSM_LEVEL = "par21";
    /**
     * Key for sending state of external charge (present or not) to server.
     * Data sends as long int value
     */
    private static final String PARAM_KEY_CHARGING_STATE = "par66";

    /**
     * Type of data about value of battery (1 -- long/int type)
     */
    private static final int PARAM_TYPE_BATTERY_LEVEL = 1;
    /**
     * Type of data about GSM level (1 -- long/int type)
     */
    private static final int PARAM_TYPE_GSM_LEVEL = 1;
    /**
     * Type of data about charging state (1 -- long/int type)
     */
    private static final int PARAM_TYPE_CHARGING_STATE = 1;
    /**
     * Max value of bus voltage. This value is hardcoded and will not be changed
     */
    private static final int MAX_BUS_VOLTAGE = 12448;

    /**
     * @param accuracy meters, size of radius around of your real location.
     * @return int, count satellites, which can provides you passed accuracy
     */
    public static int getSatellitesByAccuracy(int accuracy) {
        if ((accuracy >  0)  && (accuracy <  2))  return 17;
        if ((accuracy >= 2)  && (accuracy <  4))  return 15;
        if ((accuracy >= 4)  && (accuracy <  6))  return 13;
        if ((accuracy >= 6)  && (accuracy <  10)) return 9;
        if ((accuracy >= 10) && (accuracy <  14)) return 8;
        if ((accuracy >= 14) && (accuracy <  17)) return 7;
        if ((accuracy >= 17) && (accuracy <  20)) return 6;
        if ((accuracy >= 20) && (accuracy <  22)) return 5;
        if ((accuracy >= 22) && (accuracy <  25)) return 4;
        if ((accuracy >= 25) && (accuracy <= 40)) return 3;
        return 2;
    }

    /**
     * @param oldLocation     old position of device, which is base for future calculations
     *                        of angle between this old point and current position of this device
     * @param currentLocation position of the device, in which it is now
     * @return angle between to locations (before and current, for example)
     */
    public static int getAngleBetweenPoints(Location oldLocation, Location currentLocation) {
        int mCurrentAngle = (int) oldLocation.bearingTo(currentLocation);
        return (mCurrentAngle < 0) ? 180 + (180 - Math.abs(mCurrentAngle)) : mCurrentAngle;
    }

    /**
     * @param trackerPoints list with points, filtered by tracker
     * @return new instance of wialon blackbox package, which is prepared to sending to server
     */
    public static BlackBoxPackageRequest getBlackBoxPackage(List<TrackerPoint> trackerPoints) {
        List<DataPackageRequest> dataPackageRequests = new ArrayList<>();
        for (TrackerPoint point : trackerPoints) {
            dataPackageRequests.add(new DataPackageRequest(
                    point.getCurrentDateStamp(),
                    point.getLatitude(),
                    point.getLongitude(),
                    point.getSpeed(),
                    point.getCourse(),
                    0,
                    point.getSatellites(),
                    0.0F,
                    0,
                    0,
                    "",
                    "",
                    getParamsFromDeviceInfo(point.getDeviceInfo())
            ));
        }
        return new BlackBoxPackageRequest<>(dataPackageRequests);
    }

    /**
     * Creates new list with {@link Param}s for current tracker point: GSM-state, Battery-level, charging-state etc
     *
     * @param status POJO with status device's modules (battery, GSM)
     * @return list with {@link Param}s for tracker or empty list, if something was wrong
     */
    public static List<Param> getParamsFromDeviceInfo(DeviceInfo status) {
        // par21: Int    GSM
        // par66: Long   external charge
        // par67: Long   battery level
        // par21:1:25,par66:2:1,par67:2:5.73

        if (null == status) return new ArrayList<>();
        Param batteryType = getParamForBatteryType(status);
        Param chargingState = getParamForChargingState(status);
        Param gsmState = getParamForGsmState(status);
        return Arrays.asList(batteryType, chargingState, gsmState);
    }

    /**
     * @param status status of device with info about gsm and another (f.ex. battery...)
     * @return {@link Param} with info about gsm level
     */
    private static Param getParamForGsmState(DeviceInfo status) {
        int gsmLevel = 1;
        if ((99 == status.getGsmStrength()) || (status.getGsmStrength() <= 6)) gsmLevel = 1;
        else if (status.getGsmStrength() <= 13) gsmLevel = 2;
        else if (status.getGsmStrength() <= 20) gsmLevel = 3;
        else if (status.getGsmStrength() <= 27) gsmLevel = 4;
        else if (status.getGsmStrength() <= 31) gsmLevel = 5;
        return new Param(PARAM_KEY_GSM_LEVEL, PARAM_TYPE_GSM_LEVEL, String.valueOf(gsmLevel));
    }

    /**
     * @param status status of device with info about battery charging state
     * @return {@link Param} with info about charging state
     */
    private static Param getParamForChargingState(DeviceInfo status) {
        String chargingState = String.valueOf(status.getBatteryInfo().isCharging() ? MAX_BUS_VOLTAGE : 0);
        return new Param(PARAM_KEY_CHARGING_STATE, PARAM_TYPE_CHARGING_STATE, chargingState);
    }

    /**
     * @param status status of device with info about battery level
     * @return {@link Param} with info about battery level
     */
    private static Param getParamForBatteryType(DeviceInfo status) {
        int batteryLevel = MAX_BUS_VOLTAGE * (status.getBatteryInfo().getBatteryLevel() / 100);
        return new Param(PARAM_KEY_BATTERY_LEVEL, PARAM_TYPE_BATTERY_LEVEL, String.valueOf(batteryLevel));
    }

}
