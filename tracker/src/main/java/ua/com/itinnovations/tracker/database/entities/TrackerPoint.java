package ua.com.itinnovations.tracker.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.Calendar;

import ua.com.itinnovations.deviceinfo.DeviceInfo;
import ua.com.itinnovations.deviceinfo.battery.BatteryInfo;

/**
 * Describes tracker's DB table `tracker_points`. When tracker filters new location,
 * it store at the database. After this, runs special sender-receiver (Wialon),
 * which sends tracker's points to server.
 */
@Entity(tableName = "tracker_points")
public class TrackerPoint {

    @PrimaryKey(autoGenerate = true)
    private long mId;
    @ColumnInfo(name = "is_sent")
    private boolean mSent;
    @ColumnInfo(name = "longitude")
    private double mLongitude;
    @ColumnInfo(name = "latitude")
    private double mLatitude;
    @ColumnInfo(name = "datestamp")
    private Calendar mCurrentDateStamp;
    @ColumnInfo(name = "speed")
    private int mSpeed;
    @ColumnInfo(name = "course")
    private int mCourse;
    @ColumnInfo(name = "satellites")
    private int mSatellites;
    @ColumnInfo(name = "hdop")
    private double mHDop;
    @ColumnInfo(name = "inputs")
    private int mInputs;
    @ColumnInfo(name = "outputs")
    private int mOutputs;
    @ColumnInfo(name = "ads")
    private int mAds;
    @ColumnInfo(name = "ibuttons")
    private String mIButtons;
    /**
     * Level of battery from 0 to 100
     */
    @ColumnInfo(name = "battery_level")
    private int mBatteryLevel;
    /**
     * Flag, is device plugged in or not
     */
    @ColumnInfo(name = "is_charging")
    private boolean mCharging;
    /**
     * Level of GSM
     */
    @ColumnInfo(name = "gsm_level")
    private int mGsmLevel;


    @Deprecated
    public TrackerPoint() {
        this(0, 0.0, 0.0, Calendar.getInstance(), 0, 0, 0, 0.0F, 0, 0, 0, "", 0, false, 0);
    }

    @Ignore
    public TrackerPoint(double longitude,
                        double latitude,
                        Calendar currentDateStamp,
                        int speed,
                        int course,
                        int satellites
    ) {
        this(0, longitude, latitude, currentDateStamp, speed, course, satellites, 0.0F, 0, 0, 0, "", 0, false, 0);
    }

    @Deprecated
    public TrackerPoint(long id,
                        double longitude,
                        double latitude,
                        Calendar currentDateStamp,
                        int speed,
                        int course,
                        int satellites,
                        double HDop,
                        int inputs,
                        int outputs,
                        int ads,
                        String IButtons,
                        int batteryLevel,
                        boolean charging,
                        int gsmLevel
    ) {
        mId = id;
        mSent = false;
        mLongitude = longitude;
        mLatitude = latitude;
        mCurrentDateStamp = currentDateStamp;
        mSpeed = speed;
        mCourse = course;
        mSatellites = satellites;
        mHDop = HDop;
        mInputs = inputs;
        mOutputs = outputs;
        mAds = ads;
        mIButtons = IButtons;
        mBatteryLevel = batteryLevel;
        mCharging = charging;
        mGsmLevel = gsmLevel;
    }

    public DeviceInfo getDeviceInfo() {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setBatteryInfo(new BatteryInfo(mBatteryLevel, mCharging, -1));
        deviceInfo.setGsmStrength(mGsmLevel);
        return deviceInfo;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public boolean isSent() {
        return mSent;
    }

    public void setSent(boolean sent) {
        mSent = sent;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public Calendar getCurrentDateStamp() {
        return mCurrentDateStamp;
    }

    public void setCurrentDateStamp(Calendar currentDateStamp) {
        mCurrentDateStamp = currentDateStamp;
    }

    public int getSpeed() {
        return mSpeed;
    }

    public void setSpeed(int speed) {
        mSpeed = speed;
    }

    public int getCourse() {
        return mCourse;
    }

    public void setCourse(int course) {
        mCourse = course;
    }

    public int getSatellites() {
        return mSatellites;
    }

    public void setSatellites(int satellites) {
        mSatellites = satellites;
    }

    public double getHDop() {
        return mHDop;
    }

    public void setHDop(double HDop) {
        mHDop = HDop;
    }

    public int getInputs() {
        return mInputs;
    }

    public void setInputs(int inputs) {
        mInputs = inputs;
    }

    public int getOutputs() {
        return mOutputs;
    }

    public void setOutputs(int outputs) {
        mOutputs = outputs;
    }

    public int getAds() {
        return mAds;
    }

    public void setAds(int ads) {
        mAds = ads;
    }

    public String getIButtons() {
        return mIButtons;
    }

    public void setIButtons(String IButtons) {
        mIButtons = IButtons;
    }

    public int getBatteryLevel() {
        return mBatteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        mBatteryLevel = batteryLevel;
    }

    public boolean isCharging() {
        return mCharging;
    }

    public void setCharging(boolean charging) {
        mCharging = charging;
    }

    public int getGsmLevel() {
        return mGsmLevel;
    }

    public void setGsmLevel(int gsmLevel) {
        mGsmLevel = gsmLevel;
    }

}
