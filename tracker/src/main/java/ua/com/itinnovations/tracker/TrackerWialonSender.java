package ua.com.itinnovations.tracker;

import android.os.HandlerThread;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import ua.com.itinnovations.logger.Logger;
import ua.com.itinnovations.tracker.database.dao.TrackerPointDao;
import ua.com.itinnovations.tracker.database.entities.TrackerPoint;
import ua.com.itinnovations.tracker.utils.TrackerUtils;
import ua.com.itinnovations.wialonclient.client.WialonClient;
import ua.com.itinnovations.wialonclient.packages.Package;
import ua.com.itinnovations.wialonclient.packages.request.BlackBoxPackageRequest;
import ua.com.itinnovations.wialonclient.packages.response.BlackBoxPackageResponse;

import static ua.com.itinnovations.wialonclient.client.WialonClient.TRACKER_CONSUMER_NAME;

/**
 * This class is necessary for sending tracker's filtered points to server,
 * if points ready-to-send will absent, or sends new points. If points
 * successfully sent, it will be update their status in database.
 *
 * @author sergey
 * @since 02/03/18 11:13 AM
 */
public class TrackerWialonSender extends HandlerThread {

    private static final long ONE_SECOND_IN_MILLIS = 1000L;
    private static final String TRACKER_SENDER_THREAD_NAME = "TrackerWialonSender";

    private Config mConfig;
    private WialonClient mWialonClient;
    private TrackerPointDao mTrackerPointDao;
    private List<TrackerPoint> mLastPreparedToSendingPoints;
    private Timer mSendScheduler;
    private Timer mResponseWaiter;
    private Semaphore mSendSemaphore;
    private Logger mLogger;

    public TrackerWialonSender() {
        super(TRACKER_SENDER_THREAD_NAME);
    }

    public TrackerWialonSender(Config config,
                               WialonClient wialonClient,
                               TrackerPointDao trackerPointDao,
                               Logger logger) {
        super(TRACKER_SENDER_THREAD_NAME);
        mConfig = config;
        mLogger = logger;
        mWialonClient = wialonClient;
        mSendSemaphore = new Semaphore(1, true);
        mTrackerPointDao = trackerPointDao;
        mLastPreparedToSendingPoints = new ArrayList<>();
        mWialonClient.addReceiver(TRACKER_CONSUMER_NAME, this::handleResponseOfTrackerSending);
    }

    @Override
    public void onLooperPrepared() {
        scheduleSending(0L);
    }

    /**
     * Receiver of response from server about receiving points by server
     * @param aPackage response with count prepared points
     */
    private void handleResponseOfTrackerSending(Package aPackage) {
        BlackBoxPackageResponse blackBoxResponse = (BlackBoxPackageResponse) aPackage;
        long fromId = mLastPreparedToSendingPoints.get(0).getId();
        long countReceivedPackages = fromId + blackBoxResponse.getCountReceivedPackages();
        logResponseFromServer(countReceivedPackages);
        mTrackerPointDao.updateSentStatus(fromId, countReceivedPackages);
        mTrackerPointDao.deleteAlreadySentPointsFromDatabase();
        mResponseWaiter.cancel();
        mSendSemaphore.release();
    }

    private void logResponseFromServer(long countReceivedPackages) {
        mLogger.i(TRACKER_SENDER_THREAD_NAME, "server received %d points", countReceivedPackages);
    }

    private void scheduleSending(long sendingInterval) {
        mSendScheduler = new Timer();
        mSendScheduler.schedule(getSenderTask(), sendingInterval);
    }

    /**
     * @return instance of {@link TimerTask}, which will sends new points to server
     *         by timer
     */
    private TimerTask getSenderTask() {
        return new TimerTask() {
            @Override
            public void run() {
                sendPointsToServer();
            }
        };
    }

    private void sendPointsToServer() {
        if (!mWialonClient.isStubbed() && mWialonClient.isConnectedToServer()) {
            sendNewPointsToServer();
        } else {
            mLogger.i(TRACKER_SENDER_THREAD_NAME, "server is disconnected");
        }
        scheduleSending(getSendPeriod());
    }

    private void sendNewPointsToServer() {
        List<TrackerPoint> notSentPoints = mTrackerPointDao.getNotSentLocationsFromDatabase();
        if (!notSentPoints.isEmpty()) {
            trySendPreparedPoints(notSentPoints);
        } else {
            mLogger.i(TRACKER_SENDER_THREAD_NAME, "there are not points to sending");
        }
    }

    private List<List<TrackerPoint>> splitTrackerPointsToChunks(List<TrackerPoint> notSentPoints) {
        List<List<TrackerPoint>> splitPointsList = new ArrayList<>();
        int maxPointsInPackage = mConfig.getMaxBlackBoxPackageSize();
        for (int start = 0; start < notSentPoints.size(); start += maxPointsInPackage) {
            int end = Math.min(start + maxPointsInPackage, notSentPoints.size());
            List<TrackerPoint> chunks = notSentPoints.subList(start, end);
            splitPointsList.add(chunks);
        }
        return splitPointsList;
    }

    private void trySendPreparedPoints(List<TrackerPoint> notSentPoints) {
        List<List<TrackerPoint>> trackerPoints = splitTrackerPointsToChunks(notSentPoints);
        AtomicInteger sendingPackageIndex = new AtomicInteger(-1);
        while (sendingPackageIndex.get() < trackerPoints.size()) {
            try {
                mSendSemaphore.acquire();
            } catch (InterruptedException e) {
                Log.wtf(TRACKER_SENDER_THREAD_NAME, "some fuckup acquired, and semaphore not locked");
            }
            sendingPackageIndex.incrementAndGet();
            if (sendingPackageIndex.get() < trackerPoints.size()) {
                sendPreparedPointsToServer(trackerPoints, sendingPackageIndex);
            } else {
                mSendSemaphore.release();
            }
        }
    }

    private void sendPreparedPointsToServer(List<List<TrackerPoint>> trackerPoints, AtomicInteger packageIndex) {
        mLastPreparedToSendingPoints = trackerPoints.get(packageIndex.get());
        BlackBoxPackageRequest blackBoxPackage = TrackerUtils.getBlackBoxPackage(mLastPreparedToSendingPoints);
        mLogger.i(TRACKER_SENDER_THREAD_NAME, "tracker send package %s", blackBoxPackage);
        mWialonClient.send(blackBoxPackage);
        scheduleResponseWaiterResetTimer(packageIndex);
    }

    private void scheduleResponseWaiterResetTimer(AtomicInteger sendingPackageIndex) {
        mResponseWaiter = new Timer("TimeoutWaiter");
        mResponseWaiter.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.wtf(TRACKER_SENDER_THREAD_NAME, "some fuckup on server acquired");
                sendingPackageIndex.decrementAndGet();
                mSendSemaphore.release();
            }
        }, 15 * ONE_SECOND_IN_MILLIS);
    }

    private long getSendPeriod() {
        return mConfig.getSendPeriod() * ONE_SECOND_IN_MILLIS;
    }

}
