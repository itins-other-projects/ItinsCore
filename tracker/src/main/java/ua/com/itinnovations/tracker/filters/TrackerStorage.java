package ua.com.itinnovations.tracker.filters;

import android.location.Location;

/**
 * Common storage for all tracker filters. Here will be common variables,
 * about which needs to know every tracker filter (like before location or before stored location)
 *
 * @author sergey
 * @since 02/03/18 11:01 AM
 */
public class TrackerStorage {

    /**
     * Location, in which device was second before
     */
    private Location mBeforeLocation;

    /**
     * Location, which is stored in tracker database before
     */
    private Location mBeforeStoredLocation;


    public Location getBeforeLocation() {
        return mBeforeLocation;
    }

    public Location getBeforeStoredLocation() {
        return mBeforeStoredLocation;
    }

    public void setBeforeLocation(Location beforeLocation) {
        mBeforeLocation = beforeLocation;
    }

    public void setBeforeStoredLocation(Location beforeStoredLocation) {
        mBeforeStoredLocation = beforeStoredLocation;
    }

}
