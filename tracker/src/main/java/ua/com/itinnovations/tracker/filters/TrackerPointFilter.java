package ua.com.itinnovations.tracker.filters;

import android.location.Location;

/**
 * Describes tracker points filter, which filter points emitted by tracker.
 * All filters, which will be realised in the tracker, need to implements method
 * {@link TrackerPointFilter#handleLocation(Location)} and handle new point in it.
 *
 * @author sergey
 * @since 02/03/18 11:11 AM
 */
public interface TrackerPointFilter {

    /**
     * Handle new point. If filter predicate returns true, then add new point to storage.
     *
     * @param location new location, which necessary filter before adding to tracker storage
     */
    boolean handleLocation(Location location);

    /**
     * Describes event of successfully storing location at the database
     */
    interface OnPointSuccessfullyAdded {

        /**
         * Provides, when location successfully stored at the database
         * @param location stored location
         */
        void onAdded(Location location);

    }

}
