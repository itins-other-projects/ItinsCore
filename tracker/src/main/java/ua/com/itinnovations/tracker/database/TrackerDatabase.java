package ua.com.itinnovations.tracker.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import ua.com.itinnovations.tracker.database.converters.CalendarConverter;
import ua.com.itinnovations.tracker.database.dao.TrackerPointDao;
import ua.com.itinnovations.tracker.database.entities.TrackerPoint;

/**
 * This class describes tracker database
 */
@Database(entities = { TrackerPoint.class }, version = TrackerDatabase.VERSION)
@TypeConverters({ CalendarConverter.class })
public abstract class TrackerDatabase extends RoomDatabase {

    public static final String NAME = "TrackerDatabase";
    public static final int VERSION = 1;

    public abstract TrackerPointDao getTrackerPointDao();

}
