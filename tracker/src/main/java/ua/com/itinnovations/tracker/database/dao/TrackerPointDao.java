package ua.com.itinnovations.tracker.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import ua.com.itinnovations.tracker.database.entities.TrackerPoint;

/**
 * Provides locations, stored at the tracker database
 */
@Dao
public interface TrackerPointDao {

    /**
     * Saves new point in database
     * @param trackerPoint, new point which will be saved
     */
    @Insert
    void addNewTrackerPoint(TrackerPoint trackerPoint);

    /**
     * Selects all points, which was not sent to server
     * @return list with points, prepared by tracker for sending to server
     */
    @Query("select * from `tracker_points` where `is_sent` = '0' ")
    List<TrackerPoint> getNotSentLocationsFromDatabase();

    /**
     * Updates stored points, where id >= `from` and id <= `from + count`
     *
     * @param from  start id, from which need to update rows
     * @param count count rows, which needs to update
     */
    @Query("update `tracker_points` set `is_sent` = '1' where `mId` >= :from and `mId` <= :count")
    void updateSentStatus(long from, long count);

    /**
     * Deletes all points, which are already sent to server
     */
    @Query("delete from `tracker_points` where `is_sent` = '1'")
    void deleteAlreadySentPointsFromDatabase();

}
