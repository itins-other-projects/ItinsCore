package ua.com.itinnovations.tracker.filters.impl;

import android.location.Location;

import ua.com.itinnovations.tracker.database.dao.TrackerPointDao;
import ua.com.itinnovations.tracker.DeviceInfoInteractor;
import ua.com.itinnovations.tracker.filters.base.AbstractTrackerPointFilter;
import ua.com.itinnovations.tracker.filters.TrackerStorage;

/**
 * This filter based on time algorithm. If time between current location
 * and before location is bigger than specialized number, then needs to
 * store new point
 *
 * @author sergey
 * @since 02/03/18 11:10 AM
 */
public class TimerTrackerPointFilterImpl extends AbstractTrackerPointFilter {

    /**
     * Max time between to locations, with which new location not be added to storage
     */
    private long mMaxTimeBetweenTrackerPoints;

    /**
     * Time of adding before location. Necessary for calculating time between
     * current location and before stored location
     */
    private long mBeforeTime = -1;

    private TimerTrackerPointFilterImpl() {
        super();
    }

    public TimerTrackerPointFilterImpl(long maxTimeBetweenTrackerPoints,
                                       TrackerStorage trackerStorage,
                                       DeviceInfoInteractor deviceStateInteractor,
                                       TrackerPointDao trackerPointDao) {
        super(trackerStorage, deviceStateInteractor, trackerPointDao);
        mMaxTimeBetweenTrackerPoints = maxTimeBetweenTrackerPoints;
    }

    /**
     * Set new value of timer. It should be changed, if vehicle change
     * its `movement` state to `still`
     *
     * @param maxTimeBetweenTrackerPoints new value of timer
     */
    public void setMaxTimeBetweenTrackerPoints(long maxTimeBetweenTrackerPoints) {
        mMaxTimeBetweenTrackerPoints = maxTimeBetweenTrackerPoints;
    }

    /**
     * Handle new point. If filter predicate returns true, then add new point to storage.
     *
     * @param location new location, which necessary filter before adding to tracker storage
     */
    @Override
    public boolean handleLocation(Location location) {
        setFirstTrackerLocationIfItNull(location);
        initBeforeTimeIfItNotInitialised();
        return tryToStoreNewLocation(location);
    }

    /**
     * If before time not set (-1), then init it as current timestamp in millis
     */
    private void initBeforeTimeIfItNotInitialised() {
        if (-1 == mBeforeTime) {
            mBeforeTime = System.currentTimeMillis();
        }
    }

    /**
     * Collect data for new location and store it in tracker storage
     *
     * @param location new location, which will be stored in tracker storage
     */
    private boolean tryToStoreNewLocation(Location location) {
        long currentTime = System.currentTimeMillis();
        if (isCurrentTimeBiggerThenMaxPossible(currentTime)) {
            collectAllDataForPoint(location);
            mBeforeTime = currentTime;
            return true;
        }
        return false;
    }

    /**
     * Filter predicate
     *
     * @param currentTime current time in millis
     * @return true, if current time bigger than before stored time on max possible value
     */
    private boolean isCurrentTimeBiggerThenMaxPossible(long currentTime) {
        return (currentTime - mBeforeTime) >= mMaxTimeBetweenTrackerPoints;
    }

}
