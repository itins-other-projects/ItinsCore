package ua.com.itinnovations.tracker.filters.impl;

import android.location.Location;

import ua.com.itinnovations.tracker.database.dao.TrackerPointDao;
import ua.com.itinnovations.tracker.DeviceInfoInteractor;
import ua.com.itinnovations.tracker.filters.base.AbstractTrackerPointFilter;
import ua.com.itinnovations.tracker.filters.TrackerStorage;
import ua.com.itinnovations.tracker.utils.TrackerUtils;

/**
 * This filter stores locations, if and only if, difference between bearing of current location
 * and before location is bigger then max possible value
 *
 * @author sergey
 * @since 02/03/18 11:14 AM
 */
public class BearingTrackerPointFilterImpl extends AbstractTrackerPointFilter {

    /**
     * Max rotation value, after which, this filter stores new location
     */
    private int mMaxAngleRotation = 30;

    /**
     * Angle, which was on before location
     */
    private int mBeforeAngle = -1;

    private BearingTrackerPointFilterImpl() {
        super();
    }

    public BearingTrackerPointFilterImpl(int maxAngleRotation,
                                         TrackerStorage storage,
                                         DeviceInfoInteractor deviceStateInteractor,
                                         TrackerPointDao trackerPointDao) {
        super(storage, deviceStateInteractor, trackerPointDao);
        mMaxAngleRotation = maxAngleRotation;
    }

    /**
     * Handle new point. If filter predicate returns true, then add new point to storage.
     *
     * @param location new location, which necessary filter before adding to tracker storage
     */
    @Override
    public boolean handleLocation(Location location) {
        setBeforeLocationIfNotInitialised(location);
        setFirstTrackerLocationIfItNull(location);
        initBeforeAngleIfItNotInitialised(location);
        return tryToStoreNewLocation(location);
    }

    /**
     * Set before location, in which vehicle was
     *
     * @param location location, where vehicle was
     */
    private void setBeforeLocationIfNotInitialised(Location location) {
        if (null == mTrackerStorage.getBeforeLocation()) {
            mTrackerStorage.setBeforeLocation(location);
        }
    }

    /**
     * Initialize before angle (for before point)
     *
     * @param location location, for which needs to init angle
     */
    private void initBeforeAngleIfItNotInitialised(Location location) {
        if (-1 == mBeforeAngle) {
            mBeforeAngle = TrackerUtils.getAngleBetweenPoints(mTrackerStorage.getBeforeLocation(), location);
        }
    }

    /**
     * Collect data for new location and store it in tracker storage
     *
     * @param location this location will be stored in tracker storage, if predicate returns true
     */
    private boolean tryToStoreNewLocation(Location location) {
        int currentAngle = TrackerUtils.getAngleBetweenPoints(mTrackerStorage.getBeforeLocation(), location);
        if (isBearingToNewLocationBiggerThenMaxAngle(currentAngle)) {
            return handleSuccessfulStoringNewLocation(location, currentAngle);
        }
        return handleDismissingLocation(location, currentAngle);
    }

    /**
     * Store new location and set new value of {@link BearingTrackerPointFilterImpl#mBeforeAngle}
     *
     * @param location     new location, which stores at tracker database
     * @param currentAngle new value for {@link BearingTrackerPointFilterImpl#mBeforeAngle}
     * @return true, cuz storing is success
     */
    private boolean handleSuccessfulStoringNewLocation(Location location, int currentAngle) {
        collectAllDataForPoint(location);
        mBeforeAngle = currentAngle;
        return true;
    }

    /**
     * Set new value of before location and set new value of @link BearingTrackerPointFilter#mBeforeAngle}
     *
     * @param location     location, where the vehicle was second ago
     * @param currentAngle new value for {@link BearingTrackerPointFilterImpl#mBeforeAngle}
     * @return false, cuz location not stored
     */
    private boolean handleDismissingLocation(Location location, int currentAngle) {
        mBeforeAngle = currentAngle;
        mTrackerStorage.setBeforeLocation(location);
        return false;
    }

    /**
     * Predicate, which returns true, if difference between current and before angles
     * bigger than max possible value
     *
     * @param currentAngle value of current angle
     * @return true, if difference bigger then max possible value
     */
    private boolean isBearingToNewLocationBiggerThenMaxAngle(int currentAngle) {
        return Math.abs(mBeforeAngle - currentAngle) >= mMaxAngleRotation;
    }

}
