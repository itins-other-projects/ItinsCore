package ua.com.itinnovations.tracker.database.converters;

import android.arch.persistence.room.TypeConverter;

import java.util.Calendar;

/**
 * Converter for {@link Calendar} field. Convert it to unix timestamp and
 * store at the database, and when take field from it, convert to instance
 */
public class CalendarConverter {

    @TypeConverter
    public Calendar getCalendarFromTimestamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        return calendar;
    }

    @TypeConverter
    public long getTimestampFromCalendar(Calendar timestamp) {
        return (null == timestamp) ? 0 : timestamp.getTimeInMillis();
    }

}
