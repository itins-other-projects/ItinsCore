package ua.com.itinnovations.tracker;

import ua.com.itinnovations.deviceinfo.DeviceInfo;
import ua.com.itinnovations.deviceinfo.DeviceInfoProvider;

public class DeviceInfoInteractor implements DeviceInfoProvider {

    private DeviceInfoProvider mBatteryInfoProvider;
    private DeviceInfoProvider mGsmInfoProvider;

    public DeviceInfoInteractor(DeviceInfoProvider batteryInfoProvider,
                                DeviceInfoProvider gsmInfoProvider) {
        mBatteryInfoProvider = batteryInfoProvider;
        mGsmInfoProvider = gsmInfoProvider;
    }

    @Override
    public DeviceInfo requestDeviceInfo() {
        DeviceInfo batteryInfo = mBatteryInfoProvider.requestDeviceInfo();
        DeviceInfo gsmInfo = mGsmInfoProvider.requestDeviceInfo();
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setGsmStrength(gsmInfo.getGsmStrength());
        deviceInfo.setBatteryInfo(batteryInfo.getBatteryInfo());
        return deviceInfo;
    }

}
