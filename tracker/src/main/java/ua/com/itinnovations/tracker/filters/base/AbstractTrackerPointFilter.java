package ua.com.itinnovations.tracker.filters.base;

import android.location.Location;

import java.util.Calendar;
import java.util.TimeZone;

import ua.com.itinnovations.deviceinfo.DeviceInfo;
import ua.com.itinnovations.tracker.database.dao.TrackerPointDao;
import ua.com.itinnovations.tracker.database.entities.TrackerPoint;
import ua.com.itinnovations.tracker.DeviceInfoInteractor;
import ua.com.itinnovations.tracker.filters.TrackerPointFilter;
import ua.com.itinnovations.tracker.filters.TrackerStorage;

import static ua.com.itinnovations.tracker.utils.TrackerUtils.getAngleBetweenPoints;
import static ua.com.itinnovations.tracker.utils.TrackerUtils.getSatellitesByAccuracy;

/**
 * Implement {@link TrackerPointFilter} interface and describes some useful fields and methods.
 *
 * @author sergey
 * @since 02/03/18 11:10 AM
 */
abstract public class AbstractTrackerPointFilter implements TrackerPointFilter {

    /**
     * Storage with common trackers data
     */
    protected TrackerStorage mTrackerStorage;

    /**
     * Interactor, which provides information about device (GSM, battery...)
     */
    private DeviceInfoInteractor mDeviceStateInteractor;

    /**
     * Dao interface for inserting filtered point in database
     */
    private TrackerPointDao mTrackerPointDao;

    public AbstractTrackerPointFilter() {
    }

    public AbstractTrackerPointFilter(TrackerStorage trackerStorage,
                                      DeviceInfoInteractor deviceStateInteractor,
                                      TrackerPointDao trackerPointDao) {
        mTrackerStorage = trackerStorage;
        mDeviceStateInteractor = deviceStateInteractor;
        mTrackerPointDao = trackerPointDao;
    }

    /**
     * Collect all necessary info for current point before storing this point in storage.
     * Here collect data about current location, timestamp, speed, count satellites etc.
     *
     * @param location new point, for which needs to collect data
     */
    protected void collectAllDataForPoint(Location location) {
        saveNewPoint(location);
        mTrackerStorage.setBeforeStoredLocation(location);
    }

    /**
     * Set initial value of first point, when first point not initialised
     *
     * @param location new location, which emitted byt tracker
     */
    protected void setFirstTrackerLocationIfItNull(Location location) {
        if (null == mTrackerStorage.getBeforeStoredLocation()) {
            mTrackerStorage.setBeforeStoredLocation(location);
            saveNewPoint(location);
        }
    }

    /**
     * Save new point in database
     *
     * @param location fow which needs to collect data and store them as tracker point
     * @return {@link TrackerPoint}, which stored at tracker database
     */
    private TrackerPoint saveNewPoint(Location location) {
        TrackerPoint point = getTrackerPoint(location);
        mTrackerPointDao.addNewTrackerPoint(point);
        return point;
    }

    /**
     * Collect info about device and vehicle
     *
     * @param location new location for which needs to collect data
     * @return {@link TrackerPoint} instance for storing it in tracker storage
     */
    private TrackerPoint getTrackerPoint(Location location) {
        DeviceInfo deviceStatus = getDeviceStatus();
        TrackerPoint trackerPoint = new TrackerPoint(
                location.getLongitude(),
                location.getLatitude(),
                Calendar.getInstance(TimeZone.getTimeZone("UTC")),
                (int) (location.getSpeed() * 3.6),
                getAngleBetweenPoints(mTrackerStorage.getBeforeStoredLocation(), location),
                getSatellitesByAccuracy((int) location.getAccuracy())
        );
        if (null != deviceStatus) {
            trackerPoint.setBatteryLevel(deviceStatus.getBatteryInfo().getBatteryLevel());
            trackerPoint.setCharging(deviceStatus.getBatteryInfo().isCharging());
            trackerPoint.setGsmLevel(deviceStatus.getGsmStrength());
        }
        return trackerPoint;
    }

    private DeviceInfo getDeviceStatus() {
        DeviceInfo deviceStatus = null;
        if (null != mDeviceStateInteractor) {
            deviceStatus = mDeviceStateInteractor.requestDeviceInfo();
        }
        return deviceStatus;
    }

}
