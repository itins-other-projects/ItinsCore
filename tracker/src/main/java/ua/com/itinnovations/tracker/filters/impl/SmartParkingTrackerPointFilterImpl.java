package ua.com.itinnovations.tracker.filters.impl;

import android.location.Location;

import ua.com.itinnovations.tracker.database.dao.TrackerPointDao;
import ua.com.itinnovations.tracker.DeviceInfoInteractor;
import ua.com.itinnovations.tracker.filters.base.AbstractTrackerPointFilter;
import ua.com.itinnovations.tracker.filters.TrackerStorage;

public class SmartParkingTrackerPointFilterImpl extends AbstractTrackerPointFilter {

    private long mBeforeTime = -1;

    private Location mLocation;

    private boolean mEnabled = false;

    /**
     * Max time between to locations, with which new location not be added to storage
     */
    private long mMaxTimeBetweenTrackerPoints;

    public SmartParkingTrackerPointFilterImpl(long maxTimeBetweenTrackerPoints) {
        mMaxTimeBetweenTrackerPoints = maxTimeBetweenTrackerPoints * 1000L;
    }

    public SmartParkingTrackerPointFilterImpl(long maxTimeBetweenTrackerPoints,
                                              TrackerStorage trackerStorage,
                                              DeviceInfoInteractor deviceStateInteractor,
                                              TrackerPointDao trackerPointDao) {
        super(trackerStorage, deviceStateInteractor, trackerPointDao);
        mMaxTimeBetweenTrackerPoints = maxTimeBetweenTrackerPoints;
    }


    @Override
    public boolean handleLocation(Location location) {
        if (mEnabled) {
            initLocationForSmartParking(location);
            setFirstTrackerLocationIfItNull(mLocation);
            initBeforeTimeIfItNotInitialised();
            return tryToStoreNewLocation(mLocation);
        }
        return false;
    }

    public void resetLocation() {
        mLocation = null;
    }

    public void setEnabled(boolean enabled) {
        mEnabled = enabled;
    }

    private void initLocationForSmartParking(Location location) {
        if (null == mLocation) {
            mLocation = new Location("SmartParking");
            mLocation.setAccuracy(location.getAccuracy());
            mLocation.setLongitude(location.getLongitude());
            mLocation.setLatitude(location.getLatitude());
            mLocation.setTime(location.getTime());
            mLocation.setAltitude(location.getAltitude());
            mLocation.setBearing(location.getBearing());
            mLocation.setSpeed(0F);
        }
    }

    /**
     * Collect data for new location and store it in tracker storage
     *
     * @param location new location, which will be stored in tracker storage
     */
    private boolean tryToStoreNewLocation(Location location) {
        long currentTime = System.currentTimeMillis();
        if (isCurrentTimeBiggerThenMaxPossible(currentTime)) {
            collectAllDataForPoint(location);
            mBeforeTime = currentTime;
            return true;
        }
        return false;
    }

    /**
     * If before time not set (-1), then init it as current timestamp in millis
     */
    private void initBeforeTimeIfItNotInitialised() {
        if (-1 == mBeforeTime) {
            mBeforeTime = System.currentTimeMillis();
        }
    }

    /**
     * Filter predicate
     *
     * @param currentTime current time in millis
     * @return true, if current time bigger than before stored time on max possible value
     */
    private boolean isCurrentTimeBiggerThenMaxPossible(long currentTime) {
        return (currentTime - mBeforeTime) >= mMaxTimeBetweenTrackerPoints;
    }

    public void setMaxTimeBetweenTrackerPoints(int maxTimeBetweenTrackerPoints) {
        mMaxTimeBetweenTrackerPoints = maxTimeBetweenTrackerPoints * 1000L;
    }

}
