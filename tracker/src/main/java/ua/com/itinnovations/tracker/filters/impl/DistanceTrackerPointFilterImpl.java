package ua.com.itinnovations.tracker.filters.impl;

import android.location.Location;

import ua.com.itinnovations.tracker.database.dao.TrackerPointDao;
import ua.com.itinnovations.tracker.DeviceInfoInteractor;
import ua.com.itinnovations.tracker.filters.base.AbstractTrackerPointFilter;
import ua.com.itinnovations.tracker.filters.TrackerStorage;

/**
 * This filter based on distance algorithm. If distance between current location
 * and before location is bigger than specialized number, then needs to store new point
 *
 * @author sergey
 * @since 02/03/18 11:15 AM
 */
public class DistanceTrackerPointFilterImpl extends AbstractTrackerPointFilter {

    /**
     * Max distance between points, with which new point not be added to storage
     */
    private int mFilterDistance;

    private DistanceTrackerPointFilterImpl() {
        super();
    }

    public DistanceTrackerPointFilterImpl(int filterDistance,
                                          TrackerStorage trackerStorage,
                                          DeviceInfoInteractor deviceStateInteractor,
                                          TrackerPointDao trackerPointDao) {
        super(trackerStorage, deviceStateInteractor, trackerPointDao);
        mFilterDistance = filterDistance;
    }

    /**
     * Handle new point. If filter predicate returns true, then add new point to storage.
     *
     * @param location new location, which necessary filter before adding to tracker storage
     */
    @Override
    public boolean handleLocation(Location location) {
        setFirstTrackerLocationIfItNull(location);
        return tryToStoreNewLocation(location);
    }

    /**
     * Collect data for new location and store it in tracker storage
     *
     * @param location new location, which will be stored in tracker storage
     */
    private boolean tryToStoreNewLocation(Location location) {
        if (isDistanceBiggerThanMaxPermitted(location)) {
            collectAllDataForPoint(location);
            return true;
        }
        return false;
    }

    /**
     * Filter predicate
     *
     * @param location new location, which is emitted by tracker
     * @return returns true if current distance bigger
     * then {@link DistanceTrackerPointFilterImpl#mFilterDistance}
     */
    private boolean isDistanceBiggerThanMaxPermitted(Location location) {
        return mTrackerStorage.getBeforeStoredLocation().distanceTo(location) >= mFilterDistance;
    }

}
