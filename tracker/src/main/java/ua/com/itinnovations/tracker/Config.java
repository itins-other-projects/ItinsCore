package ua.com.itinnovations.tracker;

public class Config {

    private int mAccuracy = 24;
    private int mDistance = 150;
    private int mBearing = 10;
    private int mStillTimer = 180;
    private int mMoveTimer = 60;
    private int mMinMovingSpeed = 2;

    private String mImei = "";
    private String mPassword = "";
    private String mHost = "";
    private int mPort = 0;
    private int mSendPeriod = 5;
    private int mSocketTimeout = 150;
    private int mMaxBlackBoxPackageSize = 10;

    public Config() {
    }

    public void setServerConfigurations(String imei,
                                        String password,
                                        String host,
                                        int port,
                                        int sendDelay,
                                        int socketTimeout,
                                        int maxBlackBoxPackageSize) {
        mSendPeriod = sendDelay;
        mImei = imei;
        mPassword = password;
        mHost = host;
        mPort = port;
        mSocketTimeout = socketTimeout;
        mMaxBlackBoxPackageSize = maxBlackBoxPackageSize;
    }

    public void setTrackerConfigurations(int accuracy,
                                         int distance,
                                         int bearing,
                                         int stillTimer,
                                         int moveTimer,
                                         int minMovingSpeed) {
        mAccuracy = accuracy;
        mDistance = distance;
        mBearing = bearing;
        mStillTimer = stillTimer;
        mMoveTimer = moveTimer;
        mMinMovingSpeed = minMovingSpeed;
    }

    public int getAccuracy() {
        return mAccuracy;
    }

    public int getDistance() {
        return mDistance;
    }

    public int getBearing() {
        return mBearing;
    }

    public int getStillTimer() {
        return mStillTimer;
    }

    public int getMoveTimer() {
        return mMoveTimer * 1000;
    }

    public int getMinMovingSpeed() {
        return mMinMovingSpeed;
    }

    public String getImei() {
        return mImei;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getHost() {
        return mHost;
    }

    public int getPort() {
        return mPort;
    }

    public int getSendPeriod() {
        return mSendPeriod;
    }

    public int getSocketTimeout() {
        return mSocketTimeout * 1000;
    }

    public int getMaxBlackBoxPackageSize() {
        return mMaxBlackBoxPackageSize;
    }

}
