package ua.com.itinnovations.tracker;

import android.location.Location;

import java.util.List;

import ua.com.itinnovations.logger.Logger;
import ua.com.itinnovations.logger.impl.StubbedLoggerImpl;
import ua.com.itinnovations.tracker.filters.TrackerPointFilter;
import ua.com.itinnovations.tracker.filters.impl.SmartParkingTrackerPointFilterImpl;
import ua.com.itinnovations.tracker.filters.impl.TimerTrackerPointFilterImpl;

/**
 * Main Tracker class. From here runs all tracker logic.
 *
 * @author sergey
 * @since 02/03/18 11:18 AM
 */
public class TrackerImpl implements Tracker {

    /**
     * Config, which arrives from outside of tracker and stores some configurations
     * such as max possible distance or
     */
    private Config mTrackerConfig;

    /**
     * Tracker points sender. Sends points to Wialon server
     */
    private TrackerWialonSender mTrackerWialonSender;

    /**
     * List with tracker's filters
     */
    private List<TrackerPointFilter> mTrackerPointFilters;

    /**
     * Filter-timer, filter by time between new point and latest added
     */
    private TimerTrackerPointFilterImpl mTimerTrackerPointFilter;

    /**
     * Filter, which fix on one point, where the vehicle parked and sends it
     */
    private SmartParkingTrackerPointFilterImpl mSmartParkingTrackerPointFilter;

    /**
     * Logger for logging tracker states (wrote point, info about entered point etc.)
     */
    private Logger mLogger;

    public TrackerImpl() {
        mTrackerConfig = new Config();
        mTrackerWialonSender = new TrackerWialonSender();
        mLogger = new StubbedLoggerImpl();
    }

    public TrackerImpl(Config config, TrackerWialonSender sender, Logger logger) {
        mTrackerConfig = config;
        mTrackerWialonSender = sender;
        mLogger = logger;
    }

    /**
     * Start tracker wialon sender
     */
    @Override
    public void startSender() {
        mTrackerWialonSender.start();
    }

    /**
     * Run all tracker algorithms.
     *
     * @param location new location, which need to check and store, if necessary
     */
    @Override
    public void handleLocation(Location location) {
        mLogger.i("TRACKER", "new location: %s", location.toString());
        if (location.getSpeed() > getMinMovingSpeed()) {
            runFiltersForMoveState(location);
        } else {
            runFiltersForParkState(location);
        }
    }

    public void setTrackerPointFilters(List<TrackerPointFilter> trackerPointFilters) {
        mTrackerPointFilters = trackerPointFilters;
    }

    public void setTimerTrackerPointFilter(TrackerPointFilter filter) {
        mTimerTrackerPointFilter = (TimerTrackerPointFilterImpl) filter;
    }

    public void setSmartParkingTrackerPointFilter(TrackerPointFilter filter) {
        mSmartParkingTrackerPointFilter = (SmartParkingTrackerPointFilterImpl) filter;
    }

    private double getMinMovingSpeed() {
        return mTrackerConfig.getMinMovingSpeed() / KM_PER_HOUR_TO_M_PER_SEC_COEFFICIENT;
    }

    /**
     * Runs all filters, which works at move state, when the vehicle moves as usual
     *
     * @param location new location for which needs to run filters
     */
    private void runFiltersForMoveState(Location location) {
        int moveTimer = mTrackerConfig.getMoveTimer();
        mSmartParkingTrackerPointFilter.setEnabled(false);
        mSmartParkingTrackerPointFilter.resetLocation();
        mTimerTrackerPointFilter.setMaxTimeBetweenTrackerPoints(moveTimer);
        mSmartParkingTrackerPointFilter.setMaxTimeBetweenTrackerPoints(moveTimer);
        for (TrackerPointFilter filter : mTrackerPointFilters) {
            if (filter.handleLocation(location)) break;
        }
    }

    /**
     * Runs all filters, which works at park state, when the vehicle not moves
     *
     * @param location new location for which needs to run filters
     */
    private void runFiltersForParkState(Location location) {
        int stillTimer = mTrackerConfig.getStillTimer();
        mSmartParkingTrackerPointFilter.setEnabled(true);
        mSmartParkingTrackerPointFilter.setMaxTimeBetweenTrackerPoints(stillTimer);
        mSmartParkingTrackerPointFilter.handleLocation(location);
    }

}
