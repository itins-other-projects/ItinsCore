package ua.com.itinnovations.license;

import com.google.gson.annotations.SerializedName;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

@RunWith(RobolectricTestRunner.class)
public class LicenseManagerTest {

    private LicenseManager mLicenseManager;
    private LicenseModel mLicenseModel;

    @Before
    public void setUp() throws Exception {
        try (InputStream in = getClass().getClassLoader().getResourceAsStream("license");
             InputStreamReader streamReader = new InputStreamReader(in);
             BufferedReader reader = new BufferedReader(streamReader)
        ) {
            String license = reader.readLine();
            mLicenseManager = new LicenseManager(license);
            mLicenseModel = mLicenseManager.getLicenseModel(LicenseModel.class);

        }
    }

    @Test
    public void getLicenseModel() throws Exception {
        Assert.assertNotNull(mLicenseModel.imeis);
        Assert.assertEquals(mLicenseModel.imeis.size(), 4);

        Assert.assertEquals(mLicenseModel.modules.castModule, false);
        Assert.assertEquals(mLicenseModel.modules.messagesModule, true);
        Assert.assertEquals(mLicenseModel.modules.scoreboardModule, false);
        Assert.assertEquals(mLicenseModel.modules.timetableModule, false);
        Assert.assertEquals(mLicenseModel.modules.trackerModule, true);
        Assert.assertEquals(mLicenseModel.modules.voiceModule, false);
    }

    @Test
    public void setDeviceId() throws Exception {
        mLicenseManager.setDeviceId("867576024271620");
        Assert.assertTrue(mLicenseManager.checkLicense(mLicenseModel.imeis));

        mLicenseManager.setDeviceId("11111111111111");
        Assert.assertFalse(mLicenseManager.checkLicense(mLicenseModel.imeis));
    }

    private class LicenseModel {
        @SerializedName("imeis")
        List<String> imeis;
        @SerializedName("modules")
        Modules modules;

        public LicenseModel(List<String> imeis,
                            Modules modules
        ) {
            this.imeis = imeis;
            this.modules = modules;
        }

        @Override
        public String toString() {
            return "LicenseModel{" +
                    "imeis=" + Arrays.toString(imeis.toArray()) +
                    ", modules=" + modules.toString() +
                    '}';
        }
    }

    private class Modules {
        @SerializedName("VoiceModule")
        boolean voiceModule;
        @SerializedName("CastModule")
        boolean castModule;
        @SerializedName("TrackerModule")
        boolean trackerModule;
        @SerializedName("TimetableModule")
        boolean timetableModule;
        @SerializedName("MessagesModule")
        boolean messagesModule;
        @SerializedName("ScoreboardModule")
        boolean scoreboardModule;

        public Modules(boolean voiceModule,
                       boolean castModule,
                       boolean trackerModule,
                       boolean timetableModule,
                       boolean messagesModule,
                       boolean scoreboardModule
        ) {
            this.voiceModule = voiceModule;
            this.castModule = castModule;
            this.trackerModule = trackerModule;
            this.timetableModule = timetableModule;
            this.messagesModule = messagesModule;
            this.scoreboardModule = scoreboardModule;
        }

        @Override
        public String toString() {
            return "Modules{" +
                    "voiceModule=" + voiceModule +
                    ", castModule=" + castModule +
                    ", trackerModule=" + trackerModule +
                    ", timetableModule=" + timetableModule +
                    ", messagesModule=" + messagesModule +
                    ", scoreboardModule=" + scoreboardModule +
                    '}';
        }

    }

}