package ua.com.itinnovations.license.cripto;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Provides AES-decrypt method implemented from {@link CryptoDecoder} interface.
 */
public class AesCryptoDecoderImpl extends AbstractCryptoDecoder {

    private static final String BASE_ALGORITHM_NAME = "AES";

    public AesCryptoDecoderImpl() {
        super("AES/ECB/NoPadding");
    }

    /**
     * Decrypt encrypted string with AES algorithm.
     *
     * @param encryptedValue encoded string
     * @param key            key for decrypting
     * @return decrypted json-string which present a license
     * @throws Exception when decryption is failed
     */
    @Override
    public String decrypt(String encryptedValue, String key) throws Exception {
        Cipher cipher = Cipher.getInstance(mCipherName);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(ENCODING_NAME), BASE_ALGORITHM_NAME);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        return new String(cipher.doFinal(Base64.decode(encryptedValue, Base64.DEFAULT)));
    }

}
