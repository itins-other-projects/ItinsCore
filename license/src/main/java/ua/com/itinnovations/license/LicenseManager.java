package ua.com.itinnovations.license;

import com.google.gson.Gson;

import java.util.List;

import ua.com.itinnovations.license.cripto.AesCryptoDecoderImpl;
import ua.com.itinnovations.license.cripto.CryptoDecoder;
import ua.com.itinnovations.license.cripto.DesCryptoDecoderImpl;

/**
 * Provides methods for managing license - encrypted string with modules and devices' ids.
 *
 */
public class LicenseManager {

    private String mEncodedLicense;
    private String mDeviceId;
    private Gson mGson;

    public LicenseManager(String mLicense) {
        this.mEncodedLicense = mLicense;
        mGson = new Gson();
    }

    /**
     * Set device imei for checking license
     *
     * @param deviceId string with imei-code
     */
    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    /**
     * Returns key for AES decrypting algorithm.
     *
     * @return license key for decryption algorithm.
     */
    private String getLicenseKey() {
        return "1T1Nn0V@t10N$.$3";
    }

    /**
     * Check license.
     *
     * @param identifiers list of ids, which were encrypted in license string.
     * @return If device's id is present in license string, returns true,
     * otherwise - false
     */
    public boolean checkLicense(List<String> identifiers) {
        return identifiers.contains(mDeviceId);
    }

    /**
     * Return license as POJO.
     *
     * @param <T> License POJO-class, which will be fallen with devices' ids and modules,
     *            which are included with a particular customer
     * @param cls type of license pojo, for {@link Gson} converter
     * @return pojo, which presents modules, of application
     */
    public <T> T getLicenseModel(Class<T> cls) {
        return mGson.fromJson(decryptByDES(mEncodedLicense, getLicenseKey()), cls);
    }

    /**
     * Decrypt encrypted string with AES algorithm. This method is a wrapper for
     * {@link  AesCryptoDecoderImpl} decrypt method call.
     *
     * @param input encoded string
     * @param key   key for decrypting
     * @return decrypted json-string which present a license {@see LicenseModel}
     */
    private String decryptByAES(String input, String key) {
        return decrypt(new AesCryptoDecoderImpl(), input, key);
    }

    /**
     * Decrypt encrypted string with DES algorithm. This method is a wrapper for
     * {@link  DesCryptoDecoderImpl} decrypt method call.
     *
     * @param input encoded string
     * @param key   key for decrypting
     * @return decrypted json-string which present a license {@see LicenseModel}
     */
    private String decryptByDES(String input, String key) {
        return decrypt(new DesCryptoDecoderImpl(), input, key);
    }

    /**
     * Base decrypt-wrapper. Decrypt input string with passed decoder, using key
     *
     * @param decoder object of decoder, which will decode string
     * @param input   encoded string
     * @param key     key for decrypting
     * @return decrypted json-string which present a license {@see LicenseModel}
     */
    private String decrypt(CryptoDecoder decoder, String input, String key) {
        String license = "";
        try {
            license = decoder.decrypt(input, key);
        } catch (Exception ignored) {
        }
        return license.trim();
    }

}
