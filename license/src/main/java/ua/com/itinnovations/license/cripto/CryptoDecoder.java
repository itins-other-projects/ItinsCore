package ua.com.itinnovations.license.cripto;

/**
 * An interface for all decrypt license classes. Provides special decrypt method, which
 * will returns decrypted string
 */
public interface CryptoDecoder {

    /**
     * Interface of decrypt method. Need to be implemented in classes
     *
     * @param encryptedValue encoded string
     * @param key            key for decrypting
     * @return decrypted string
     * @throws Exception when decryption is failed
     */
    String decrypt(String encryptedValue, String key) throws Exception;

}
