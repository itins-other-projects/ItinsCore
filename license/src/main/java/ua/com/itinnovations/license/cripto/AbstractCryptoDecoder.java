package ua.com.itinnovations.license.cripto;

/**
 * Base class for decrypt license classes. Need to extends from it, for
 * future working with cipher algorithms
 */
public abstract class AbstractCryptoDecoder implements CryptoDecoder {

    /**
     * Name of encoding for inputting string
     */
    static final String ENCODING_NAME = "UTF-8";

    /**
     * Name of cipher algorithm
     */
    String mCipherName;

    AbstractCryptoDecoder(String cipherName) {
        mCipherName = cipherName;
    }

}
