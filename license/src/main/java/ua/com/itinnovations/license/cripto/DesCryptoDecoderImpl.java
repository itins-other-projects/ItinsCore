package ua.com.itinnovations.license.cripto;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

/**
 * Provides DES-decrypt method implemented from {@link CryptoDecoder} interface.
 */
public class DesCryptoDecoderImpl extends AbstractCryptoDecoder {

    private static final String BASE_ALGORITHM_NAME = "DES";

    public DesCryptoDecoderImpl() {
        super("DES/ECB/NoPadding");
    }

    /**
     * Decrypt encrypted string with DES algorithm.
     *
     * @param encryptedValue encoded string
     * @param key            key for decrypting
     * @return decrypted json-string which present a license
     * @throws Exception when decryption is failed
     */
    @Override
    public String decrypt(String encryptedValue, String key) throws Exception {
        Cipher cipher = Cipher.getInstance(mCipherName);
        DESKeySpec desKeySpec = new DESKeySpec(key.getBytes(ENCODING_NAME));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(BASE_ALGORITHM_NAME);
        cipher.init(Cipher.DECRYPT_MODE, keyFactory.generateSecret(desKeySpec));
        return new String(cipher.doFinal(Base64.decode(encryptedValue, Base64.DEFAULT)));
    }

}
