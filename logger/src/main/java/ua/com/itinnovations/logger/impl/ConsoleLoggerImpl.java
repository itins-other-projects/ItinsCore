package ua.com.itinnovations.logger.impl;

import android.util.Log;

import ua.com.itinnovations.logger.AbstractLogger;

public class ConsoleLoggerImpl extends AbstractLogger {

    public ConsoleLoggerImpl(LogLevel logLevel) {
        super(logLevel);
    }

    @Override
    public void i(String tag, String message, Object... args) {
        if (isLoggingPossible(LogLevel.INFO)) {
            if (0 != args.length) {
                Log.i(tag, String.format(message, args));
            } else {
                Log.i(tag, message);
            }
        }
    }

    @Override
    public void v(String tag, String message, Object... args) {
        if (isLoggingPossible(LogLevel.VERBOSE)) {
            if (0 != args.length) {
                Log.v(tag, String.format(message, args));
            } else {
                Log.v(tag, message);
            }
        }
    }

    @Override
    public void d(String tag, String message, Object... args) {
        if (isLoggingPossible(LogLevel.DEBUG)) {
            if (0 != args.length) {
                Log.d(tag, String.format(message, args));
            } else {
                Log.d(tag, message);
            }
        }
    }

    @Override
    public void e(String tag, String message, Object... args) {
        if (isLoggingPossible(LogLevel.ERROR)) {
            if (0 != args.length) {
                Log.e(tag, String.format(message, args));
            } else {
                Log.e(tag, message);
            }
        }
    }

    @Override
    public void wtf(String tag, String message, Throwable throwable) {
        if (isLoggingPossible(LogLevel.WTF)) {
            Log.wtf(tag, message, throwable);
        }
    }

    @Override
    public void wtf(String tag, String message, Throwable throwable, Object... args) {
        if (isLoggingPossible(LogLevel.WTF)) {
            if (0 != args.length) {
                Log.wtf(tag, String.format(message, args), throwable);
            } else {
                Log.wtf(tag, message, throwable);
            }
        }
    }

}
