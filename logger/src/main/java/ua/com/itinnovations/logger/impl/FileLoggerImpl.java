package ua.com.itinnovations.logger.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import ua.com.itinnovations.logger.AbstractLogger;

public class FileLoggerImpl extends AbstractLogger {

    private static final String LOG_FILE_EXTENSION = ".log";
    private final Object mLock;
    private String mLogFileName;
    private final String mFullPathToLog;

    public FileLoggerImpl(String pathToLogDirectory, LogLevel logLevel, int maxLogPoolSize) throws Exception {
        super(logLevel);

        mLock = new Object();

        File logDirectory = new File(pathToLogDirectory);
        if (!logDirectory.exists()) {
            throw new Exception("log directory not exists");
        }

        File[] logFiles = new File(pathToLogDirectory).listFiles();
        if (logFiles.length > maxLogPoolSize) {
            Arrays.sort(logFiles);
            logFiles[logFiles.length - 1].delete();
        }

        mLogFileName = getCurrentDayAndTime();
        mFullPathToLog = getFullPathToLogFile(pathToLogDirectory);
        File fullPathToLogFile = new File(mFullPathToLog);
        if (!fullPathToLogFile.exists()) {
            fullPathToLogFile.createNewFile();
        }
    }

    @Override
    public void i(String tag, String message, Object... args) {
        log(tag, LogLevel.INFO, message, null, args);
    }

    @Override
    public void v(String tag, String message, Object... args) {
        log(tag, LogLevel.VERBOSE, message, null, args);
    }

    @Override
    public void d(String tag, String message, Object... args) {
        log(tag, LogLevel.DEBUG, message, null, args);
    }

    @Override
    public void e(String tag, String message, Object... args) {
        log(tag, LogLevel.ERROR, message, null, args);
    }

    @Override
    public void wtf(String tag, String message, Throwable throwable) {
        log(tag, LogLevel.WTF, message, throwable);
    }

    @Override
    public void wtf(String tag, String message, Throwable throwable, Object... args) {
        log(tag, LogLevel.WTF, message, throwable, args);
    }

    private void log(String tag, LogLevel logLevel, String message, Throwable throwable, Object... args) {
        if (isLoggingPossible(logLevel)) {
            if (args.length != 0) {
                write(tag, logLevel, String.format(message, args), throwable);
            } else {
                write(tag, logLevel, message, throwable);
            }
        }
    }

    private String getFullPathToLogFile(String pathToLogDirectory) {
        return String.format("%s%s%s%s", pathToLogDirectory, File.separator, mLogFileName, LOG_FILE_EXTENSION);
    }

    private String getCurrentDayAndTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        return formatter.format(Calendar.getInstance().getTime());
    }

    private void write(String tag, LogLevel logLevel, String message, Throwable throwable) {
        synchronized (mLock) {
            writeToFile(tag, logLevel, message, throwable);
        }
    }

    private void writeToFile(String tag, LogLevel logLevel, String message, Throwable throwable) {
        try (FileOutputStream logOutput = new FileOutputStream(mFullPathToLog, true)) {
            String time = mLogTimeFormatter.format(Calendar.getInstance().getTime());
            String separator = System.lineSeparator();
            String logString = String.format("%s %s/%s: %s%s", time, logLevel, tag, message, separator);
            logOutput.write(logString.getBytes());
            if (null != throwable) {
                String stackTrace = Arrays.toString(throwable.getStackTrace());
                String formattedStackTrace = String.format("%s%s", stackTrace, separator);
                logOutput.write(formattedStackTrace.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
