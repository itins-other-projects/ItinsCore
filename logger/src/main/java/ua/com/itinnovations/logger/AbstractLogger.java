package ua.com.itinnovations.logger;

import java.text.SimpleDateFormat;
import java.util.Locale;

abstract public class AbstractLogger implements Logger {

    private LogLevel mLogLevel;
    protected SimpleDateFormat mLogTimeFormatter;

    protected AbstractLogger() {
        this(LogLevel.DISABLED);
    }

    protected AbstractLogger(LogLevel logLevel) {
        mLogLevel = logLevel;
        mLogTimeFormatter = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    }

    protected boolean isLoggingPossible(LogLevel logLevel) {
        return logLevel.compareTo(mLogLevel) <= 0;
    }

}
