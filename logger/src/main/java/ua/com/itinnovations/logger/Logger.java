package ua.com.itinnovations.logger;

/**
 * Provides an API for logging app state to file.
 * For logging there're 5 levels + {@link LogLevel#DISABLED}, when logging is disabled:
 * {@link LogLevel#INFO}     - information logs, f.e. about starting and finishing app
 * {@link LogLevel#VERBOSE}  - more informative log: on this level outs information about ????
 * {@link LogLevel#DEBUG}    - debug logs: on this level will shows values of variables, equality results etc.
 * {@link LogLevel#ERROR}    - on this level will outs errors, such as file not found or object is null etc.
 * {@link LogLevel#WTF}      - on this level will outs errors and exceptions, which can crash app. They will
 *                             print not only message, but stackTrace, if it passed
 */
public interface Logger {

    void i(String tag, String message, Object ... args);

    void v(String tag, String message, Object ... args);

    void d(String tag, String message, Object ... args);

    void e(String tag, String message, Object ... args);

    void wtf(String tag, String message, Throwable throwable);

    void wtf(String tag, String message, Throwable throwable, Object ... args);


    /**
     * enum with log levels. Provides names for number-presented levels
     */
    enum LogLevel {
        DISABLED,
        INFO,
        VERBOSE,
        DEBUG,
        ERROR,
        WTF;

        @Override
        public String toString() {
            switch (this) {
                case VERBOSE: return "v";
                case DEBUG: return "d";
                case INFO: return "i";
                case ERROR: return "e";
                case WTF: return "wtf";
                default: return "";
            }
        }

    }

}
