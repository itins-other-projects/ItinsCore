package ua.com.itinnovations.logger.impl;

import ua.com.itinnovations.logger.AbstractLogger;

public class StubbedLoggerImpl extends AbstractLogger {

    public StubbedLoggerImpl() {
        super();
    }

    @Override
    public void i(String tag, String message, Object... args) {
        /* stubbed */
    }

    @Override
    public void v(String tag, String message, Object... args) {
        /* stubbed */
    }

    @Override
    public void d(String tag, String message, Object... args) {
        /* stubbed */
    }

    @Override
    public void e(String tag, String message, Object... args) {
        /* stubbed */
    }

    @Override
    public void wtf(String tag, String message, Throwable throwable) {
        /* stubbed */
    }

    @Override
    public void wtf(String tag, String message, Throwable throwable, Object... args) {
        /* stubbed */
    }

}
