package ua.com.itinnovations.socketclient.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.Socket;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Socket.class)
public class SocketClientImplTest {

    private static final int SERVER_SOCKET_PORT = 10000;
    private static final String SERVER_SOCKET_HOST = "localhost";


//    @Test
//    public void test() throws Exception {
//        SocketClient client = new SocketClientImpl(SERVER_SOCKET_HOST, SERVER_SOCKET_PORT);
//
//        client.setSocketClientStateListener(socketState -> {
//            if (SocketClient.SocketState.CONNECTED == socketState) {
//                System.out.println("connected");
//                client.write("hello, world");
//            }
//        });
//
//        Thread.sleep(10000);
//    }


    @Test
    public void testWrite() throws Exception {
        Socket socket = mock(Socket.class);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(new byte[] {  });
        when(socket.getOutputStream()).thenReturn(out);
        when(socket.getInputStream()).thenReturn(in);
        when(socket.isConnected()).thenReturn(true);
        SocketClientImpl socketClient = new SocketClientImpl(SERVER_SOCKET_HOST, SERVER_SOCKET_PORT);
        Whitebox.invokeMethod(socketClient, "initSocketIfItConnected", socket);

        String expectedValue = "Hello, world of UnitTesting" + System.lineSeparator();
        socketClient.write(expectedValue);

        Thread.sleep(2000);

        Assert.assertEquals(out.toString(), expectedValue);
    }

    @Test
    public void testConnect() throws Exception {
        Socket socket = mock(Socket.class);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(new byte[] {  });
        when(socket.getOutputStream()).thenReturn(out);
        when(socket.getInputStream()).thenReturn(in);
        when(socket.isConnected()).thenReturn(true);

        PowerMockito.whenNew(Socket.class).withArguments(SERVER_SOCKET_HOST, SERVER_SOCKET_PORT)
                .thenReturn(socket);
        SocketClientImpl socketClient = new SocketClientImpl(SERVER_SOCKET_HOST, SERVER_SOCKET_PORT);
        socketClient.setSocketClientStateListener(socketState -> {  });

        socketClient.connect();

        Thread.sleep(10000);

//        Assert.assertTrue(socketClient.isConnected());
    }

    @Test()
    public void testReconnect() throws Exception {
    }

    @Test
    public void testReadFromServer() throws Exception {
        String expectedValue = "Hello, world of UnitTesting" + System.lineSeparator();
        Socket socket = mock(Socket.class);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(expectedValue.getBytes());
        when(socket.getOutputStream()).thenReturn(out);
        when(socket.getInputStream()).thenReturn(in);
        when(socket.isConnected()).thenReturn(true);
        SocketClientImpl socketClient = new SocketClientImpl(SERVER_SOCKET_HOST, SERVER_SOCKET_PORT);

        Whitebox.invokeMethod(socketClient, "initSocketIfItConnected", socket);
        Thread.sleep(5000);
        socketClient.setConsumer(actualValue -> {
            System.out.println(actualValue);
            Assert.assertEquals(expectedValue, actualValue);
        });

        in = new ByteArrayInputStream("".getBytes());

    }

}
