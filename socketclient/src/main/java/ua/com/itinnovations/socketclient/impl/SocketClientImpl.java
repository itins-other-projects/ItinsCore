package ua.com.itinnovations.socketclient.impl;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import ua.com.itinnovations.socketclient.SocketClient;

public class SocketClientImpl implements SocketClient {

    private static final int MAX_CONNECTIONS_ATTEMPT_TIME = 15 * 1000;
    private static final int ATTEMPTING_CHECKING_TIMEOUT = 5 * 1000;
    private static final int RECONNECTING_TIMER = 60 * 1000;

    private BufferedReader mSocketReader;
    private OutputStreamWriter mSocketWriter;

    private Socket mSocket;
    private String mHost;
    private int mPort;

    private boolean mConnected;
    private boolean mReconnect;

    private SocketDataReceivedListener mDataReceivedListener;
    private SocketClientStateListener mSocketClientStateListener;

    public SocketClientImpl(String host, int port) {
        mHost = host;
        mPort = port;

        mDataReceivedListener = data -> {  };
        mSocketClientStateListener = socketState -> {  };
    }

    @Override
    public void setSocketClientStateListener(SocketClientStateListener stateListener) {
        mSocketClientStateListener = stateListener;
    }

    @Override
    public void setConsumer(SocketDataReceivedListener dataReceivedListener) {
        mDataReceivedListener = dataReceivedListener;
    }

    @Override
    public boolean isConnected() {
        return mConnected;
    }

    @Override
    public void connect() {
        mReconnect = false;
        Observable.fromCallable(() -> new Socket(mHost, mPort))
                .subscribeOn(Schedulers.io())
                .doOnNext(this::socketConnectingCallable)
                .subscribe(this::handleSocketConnectionState, this::tryToReconnectToServerSocket);
    }

    @Override
    public void write(String value) {
        Observable.fromCallable(getWriterCallable(value))
                .subscribeOn(Schedulers.io())
                .subscribe(aBoolean -> {}, this::tryToReconnectToServerSocket);
    }

    private void socketConnectingCallable(Socket socket) throws InterruptedException {
        long currentTime = System.currentTimeMillis();
        while (isConnectionAttemptingFinished(currentTime, socket)) {
            Thread.sleep(ATTEMPTING_CHECKING_TIMEOUT);
        }
    }

    private Callable<Boolean> getWriterCallable(String value) {
        return () -> {
            mSocketWriter.write(value);
            mSocketWriter.flush();
            return true;
        };
    }

    private boolean isConnectionAttemptingFinished(long currentTime, Socket socket) {
        return ((null != socket) && !socket.isConnected()) ||
                ((System.currentTimeMillis() - currentTime) >= MAX_CONNECTIONS_ATTEMPT_TIME);
    }

    private void handleSocketConnectionState(Socket socket) throws Exception {
        if (socket.isConnected()) {
            initSocketIfItConnected(socket);
            mSocketClientStateListener.onStateChanged(SocketState.CONNECTED);
        } else {
            tryToReconnectToServerSocket(null);
        }
    }

    private void initSocketIfItConnected(Socket socket) throws IOException {
        mSocket = socket;
        mConnected = true;
        InputStreamReader in = new InputStreamReader(mSocket.getInputStream());
        mSocketReader = new BufferedReader(in);
        mSocketWriter = new OutputStreamWriter(mSocket.getOutputStream());
        new Thread(this::initSocketReadingRunnable).start();
    }

    private void initSocketReadingRunnable() {
        try {
            readFromSocketInputStream();
        } catch (Exception e) {
            tryToReconnectToServerSocket(e);
        }
    }

    private void tryToReconnectToServerSocket(Throwable throwable) {
        if (!mReconnect && (throwable instanceof SocketException)) {
            mSocketClientStateListener.onStateChanged(SocketState.DISCONNECTED);
            mConnected = false;
            mReconnect = true;
            initSocketReconnection();
        }
    }

    private void readFromSocketInputStream() throws Exception {
        while (mSocket.isConnected()) {
            tryReadFromSocketInputStream();
        }
    }

    private void tryReadFromSocketInputStream() throws Exception {
        String string = mSocketReader.readLine();
        if (StringUtils.isEmpty(string)) {
            throw new Exception();
        }
        if (!StringUtils.isBlank(string)) {
            mDataReceivedListener.onDataReceived(StringEscapeUtils.unescapeJava(string));
        }
    }

    private void initSocketReconnection() {
        new Timer().schedule(getReconnectionTimerTask(), RECONNECTING_TIMER);
    }

    private TimerTask getReconnectionTimerTask() {
        return new TimerTask() {
            @Override
            public void run() {
                connect();
            }
        };
    }

}
