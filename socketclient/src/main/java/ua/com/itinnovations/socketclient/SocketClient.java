package ua.com.itinnovations.socketclient;

/**
 * Client socket interface.<br />
 * This contract is must be implemented by every client socket. The main features of this client are
 * connection, detecting socket's aborts and reconnecting socket.
 */
public interface SocketClient {

    /**
     * Enum with basic socket's states. Describe state {@link #CONNECTED}, when socket successfully
     * connected to server and {@link #DISCONNECTED}, when socket disconnect from server or lose
     * connection.<br />
     * This states will produces by socket from outside via callback and necessary only for information
     * in high-level code, that socket now connected/disconnected, so client-code can makes a decision,
     * to start working, or wait, while socket will reestablish connection.
     */
    enum SocketState {
        /**
         * When socket successfully connected to server, the callback will return this state.
         * It's mean, then socket is ready to usage, so it's possible to transfer data by it.
         */
        CONNECTED,
        /**
         * When socket loses connection with server, because loosing internet connection or
         * broke socket-pipe, this state will returns by callback.<br />
         * It's no necessary to manage socket from high-level code (f.ex. trying to reconnect).
         * {@link SocketClient} will done this itself.
         */
        DISCONNECTED
    }

    /**
     * Starts connection socket to server. This method executes in background thread, so you don't need
     * to do this yourself in your high-level code.
     */
    void connect();

    /**
     * Listener, which will notify about all changes with socket state. If it will loose connection or
     * reconnects, you can be notified, if you set this listener.
     * See details about listener in doc to it: {@link SocketClientStateListener}
     *
     * @param socketClientStateListener listener, which will be called with new {@link SocketClient} state
     */
    void setSocketClientStateListener(SocketClientStateListener socketClientStateListener);

    /**
     * Set listener, which will be called, when socket receives new data from server.
     *
     * @param dataReceivedListener
     */
    void setConsumer(SocketDataReceivedListener dataReceivedListener);

    /**
     * @param value
     */
    void write(String value);

    /**
     * @return
     */
    boolean isConnected();


    /**
     *
     */
    @FunctionalInterface
    interface SocketClientStateListener {

        /**
         * @param socketState
         */
        void onStateChanged(SocketState socketState);

    }

    /**
     * Listener of data received from server.
     */
    @FunctionalInterface
    interface SocketDataReceivedListener {

        /**
         * @param data
         */
        void onDataReceived(String data) throws Exception;

    }

}
